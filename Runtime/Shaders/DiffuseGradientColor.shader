﻿Shader "Playstrom/DiffuseGradientColor"
{
    Properties
    {
        _ColorTop("Color",COLOR)=(0.5,0.5,0.5,1.0)
        _TopPos ("Top Pos", Float) = -1
        _ColorDown("Color",COLOR)=(0.5,0.5,0.5,1.0)
        _DownPos ("Down Pos", Float) = 1
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }

    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
        }
        LOD 150
        CGPROGRAM
        #pragma surface surf Lambert noforwardadd

        sampler2D _MainTex;
        fixed4 _ColorTop;
        fixed4 _ColorDown;
        float _TopPos;
        float _DownPos;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
        };

        void surf(Input IN, inout SurfaceOutput o)
        {
            float h = (_TopPos - IN.worldPos.y) / (_TopPos - _DownPos);
            fixed4 gradColor = lerp(_ColorTop, _ColorDown, h);
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * gradColor;
            o.Albedo = c.rgb;
        }
        ENDCG
    }
    Fallback "Mobile/VertexLit"
}