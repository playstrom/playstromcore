using System.IO;
using LinqToCodedom;
using LinqToCodedom.Extensions;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;

namespace Playstrom.Core.Editor
{
    public class SettingsInitialisation : EditorWindow
    {
        [MenuItem("Tools/Playstrom/Init/Settings creator")]
        private static void OpenImportWindow()
        {
            var window = (SettingsInitialisation)GetWindow(typeof(SettingsInitialisation));
            window.Show();
        }

        private void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Class path", GUILayout.Width(100));
            m_classPath = GUILayout.TextField(m_classPath);
            GUILayout.EndHorizontal();

            ScriptableObject scriptableObj = this;
            SerializedObject serialObj = new SerializedObject(scriptableObj);
            SerializedProperty serialProp = serialObj.FindProperty("m_mainSettingsEnumValue");
            EditorGUILayout.PropertyField(serialProp, true);
            serialObj.ApplyModifiedProperties();

            GUILayout.Label("Class namespace", GUILayout.Width(200));
            m_classNamespace = GUILayout.TextField(m_classNamespace);
            GUILayout.Label("Class name", GUILayout.Width(200));
            m_className = GUILayout.TextField(m_className);

            if (GUILayout.Button("Generate settings enum"))
            {
                if (string.IsNullOrEmpty(m_className) || string.IsNullOrEmpty(m_classPath))
                {
                    return;
                }

                CompilationPipeline.compilationFinished += AssemblyCompilationFinished;
                GenerateSettingsConfig();
                CompilationPipeline.RequestScriptCompilation();

                void AssemblyCompilationFinished(object o)
                {
                    Debug.Log("Recompilation finished");
                    CompilationPipeline.compilationFinished -= AssemblyCompilationFinished;
                }
            }
        }

        private void GenerateSettingsConfig()
        {
            var endPath = Path.Combine(m_classPath, m_className + ".cs");
            var fullPath = Path.Combine(Application.dataPath, endPath);
            var code = GenerateEnum();
            CodeGenerationExtensions.WriteIntoFile(fullPath, code);
        }

        private string GenerateEnum()
        {
            var c = new CodeDomGenerator();
            var enumDeck = CodeGenerationExtensions.GetEnum(m_enumName, m_mainSettingsEnumValue);
            c.AddNamespace("Settings").AddEnum(enumDeck);

            return c.GenerateCode(CodeDomGenerator.Language.CSharp);
        }
        
        private static string m_className = "SettingsConfig";
        private static string m_classNamespace = "Settings";

        private static string m_classPath = @"Scripts/Generated";
        private static string m_enumName = "SettingsType";

        [SerializeField] private string[] m_mainSettingsEnumValue =
        {
            "Sound",
            "Music",
            "Vibro",
        };
    }
}