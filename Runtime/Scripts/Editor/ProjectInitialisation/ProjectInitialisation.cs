using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace Playstrom.Core.Editor
{
    public class ProjectInitialisation : EditorWindow
    {
        [MenuItem("Tools/Playstrom/Init/Project Base")]
        public static void CreateProjectBaseData()
        {
            CreateProjectContext();
        }

        private static void CreateProjectContext()
        {
            var projectContextPrefab = PrefabCreator.CreatePrefabToResources("ProjectContext", string.Empty);
            var context = CheckComponent<ProjectContext>(projectContextPrefab);

            var effectInstallerPrefab = CreateEffectInstaller();
            var uiInstallerPrefab = CreateUiInstaller();

            var installers = context.InstallerPrefabs.ToList();
            installers.RemoveAll(x => x == null);
            installers.Add(effectInstallerPrefab);
            installers.Add(uiInstallerPrefab);

            context.InstallerPrefabs = new List<MonoInstaller>(installers.Distinct());

            AssetDatabase.Refresh();
            Debug.Log("Project was successful configured");
        }

        private static MonoInstaller CreateEffectInstaller()
        {
            var effectsPrefab = PrefabCreator.CreatePrefabToResources("Effects", EffectPath);
            var effectsComponent = CheckComponent<Effects.Effects>(effectsPrefab);

            var installerPrefab = PrefabCreator.CreatePrefab("EffectInstaller", InstallerPath);
            var effectInstaller = CheckComponent<EffectInstaller>(installerPrefab);
            effectInstaller.SetEffects(effectsComponent);

            AssetDatabase.Refresh();
            return effectInstaller;
        }

        private static MonoInstaller CreateUiInstaller()
        {
            var prefab = PrefabCreator.CreatePrefab("UiInstaller", InstallerPath);
            var uiInstaller = CheckComponent<UiInstaller>(prefab);

            AssetDatabase.Refresh();
            return uiInstaller;
        }

        private static T CheckComponent<T>(GameObject gameObject) where T : Component
        {
            var component = gameObject.GetComponent<T>();
            if (component == null)
            {
                component = gameObject.AddComponent<T>();
            }

            return component;
        }

        private static string InstallerPath => $"/Installers";
        private static string EffectPath => "/Effects";
    }
}