using System.IO;
using System.Reflection;
using LinqToCodedom;
using LinqToCodedom.Extensions;
using UnityEditor;
using UnityEngine;

namespace Playstrom.Core.Editor
{
    public static class GameManagerGeneration
    {
        [MenuItem("Tools/Playstrom/Init/GameUIManager creator")]
        public static void Create()
        {
            const string className = "GameUIManager";
            var path = $"Scripts/UI/{className}.cs";
            var fullPath = Path.Combine(Application.dataPath, path);

            var code = GenerateClassWithConstants(className);
            CodeGenerationExtensions.WriteIntoFile(fullPath, code);
        }

        private static string GenerateClassWithConstants(string className)
        {
            var c = new CodeDomGenerator();
            c.AddNamespace("UI").Imports("Playstrom.Core.Ui").AddClass(className, TypeAttributes.Public)
                .Inherits(nameof(Ui.UiManager));
            var code = c.GenerateCode(CodeDomGenerator.Language.CSharp);
            return code;
        }
    }
}