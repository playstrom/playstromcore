using System;
using System.IO;
using System.Linq;
using System.Reflection;
using LinqToCodedom;
using LinqToCodedom.Extensions;
using Playstrom.Core.Ui;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;
using Assembly = System.Reflection.Assembly;

namespace Playstrom.Core.Editor
{
    [InitializeOnLoad]
    public class SceneCreator : EditorWindow
    {
        static SceneCreator()
        {
            EditorApplication.delayCall += CheckNeedContinueOperations;
        }

        private static void CheckNeedContinueOperations()
        {
            string val = EditorPrefs.GetString(ContinueOperationKey, null);
            if (string.IsNullOrEmpty(val))
            {
                return;
            }

            EditorPrefs.DeleteKey(ContinueOperationKey);
            var window = (SceneCreator)GetWindow(typeof(SceneCreator));
            JsonUtility.FromJsonOverwrite(val, window);
            window.DelayAfterCompilation();
        }

        [MenuItem("Tools/Playstrom/UI/Scene creator")]
        private static void OpenImportWindow()
        {
            var window = (SceneCreator)GetWindow(typeof(SceneCreator));
            window.Show();
        }

        private void OnGUI()
        {
            GUILayout.Label("Path");
            GUILayout.BeginHorizontal();
            GUILayout.Label("Scene path", GUILayout.Width(100));
            m_scenePath = GUILayout.TextField(m_scenePath);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Class path", GUILayout.Width(100));
            m_classPath = GUILayout.TextField(m_classPath);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Class name", GUILayout.Width(100));
            m_className = GUILayout.TextField(m_className);
            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            GUILayout.Label("Create scene context ?", GUILayout.Width(140));
            m_createSceneContext = GUILayout.Toggle(m_createSceneContext,"");
            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            GUILayout.Label("Create dummy ?", GUILayout.Width(140));
            m_createDummy = GUILayout.Toggle(m_createDummy,"");
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Generate panel"))
            {
                if (string.IsNullOrEmpty(m_className) || string.IsNullOrEmpty(m_classPath) ||
                    string.IsNullOrEmpty(m_scenePath)) return;

                CompilationPipeline.compilationFinished += AssemblyCompilationFinished;
                CreateClass(ClassNameScript, m_classPath);
                CreateDirectories();
                CompilationPipeline.RequestScriptCompilation();

                void AssemblyCompilationFinished(object o)
                {
                    Debug.Log("Recompilation finished");
                    var val = JsonUtility.ToJson(this);
                    EditorPrefs.SetString(ContinueOperationKey, val);
                    CompilationPipeline.compilationFinished -= AssemblyCompilationFinished;
                }
            }
        }

        private void CreateDirectories()
        {
            var spritesFolderPath = $"{Application.dataPath}/Sprites/UI/{m_className}";
            if (!Directory.Exists(spritesFolderPath))
            {
                Directory.CreateDirectory(spritesFolderPath);
                Debug.Log($"Folder was created on {spritesFolderPath}");
            }

            var animationsFolder = $"{Application.dataPath}/Animations/UI/{m_className}";
            if (!Directory.Exists(animationsFolder))
            {
                Directory.CreateDirectory(animationsFolder);
                Debug.Log($"Folder was created on {animationsFolder}");
            }

            var fullPath = $"{Application.dataPath}/{m_classPath}/{m_className}Additional";
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
                Debug.Log($"Folder was created on {fullPath}");
            }
            
            AssetDatabase.Refresh();
        }

        private void DelayAfterCompilation()
        {
            var type = GetTypeByName(ClassNameScript);
            if (type != null)
            {
                CreateScene(m_className, m_scenePath);
            }
        }

        private void CreateClass(string className, string path)
        {
            var endPath = Path.Combine(path, className + ".cs");
            var fullPath = Path.Combine(Application.dataPath, endPath);

            var code = GenerateClassWithConstants(className);
            CodeGenerationExtensions.WriteIntoFile(fullPath, code);

            Debug.Log($"Class created {ClassNameScript}");

            fullPath = fullPath.Substring(Environment.CurrentDirectory.Length);
            if (fullPath.StartsWith("\\") || fullPath.StartsWith("/"))
            {
                fullPath = fullPath.Substring(1);
            }

            AssetDatabase.ImportAsset(fullPath,
                ImportAssetOptions.ForceSynchronousImport | ImportAssetOptions.ForceUpdate);
            Debug.Log("Start recompilation...");
        }

        private static string GenerateClassWithConstants(string className)
        {
            var c = new CodeDomGenerator();
            c.AddNamespace("UI").Imports("Playstrom.Core.Ui").AddClass(className, TypeAttributes.Public)
                .Inherits(nameof(UiPanelBase));
            var code = c.GenerateCode(CodeDomGenerator.Language.CSharp);
            return code;
        }


        private void CreateScene(string sceneName, string scenePath)
        {
            Scene scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Additive);
            scene.name = $"{sceneName}";

            var folderPath = Path.Combine(Application.dataPath, scenePath);
            var endPath = Path.Combine(folderPath, $"{sceneName}.unity");
            if (!Directory.Exists(folderPath))
            {
                Debug.Log($"Create Directory {sceneName}");
                Directory.CreateDirectory(folderPath);
            }

            var root = AddCanvasComponents(sceneName);

            var type = GetTypeByName(ClassNameScript);
            if (type != null)
            {
                var component = root.AddComponent(type);
                if (component == null)
                {
                    Debug.LogError($"No Exists class {m_className}");
                }
            }

            if (m_createDummy)
            {
                Debug.Log("Create dummy");
                var dummy = AddDummy(root.transform);
                root.GetComponent<UiPanelBase>().SetDummy(dummy);
            }

            if (m_createSceneContext)
            {
                Debug.Log("Create scene context");
                var sceneContext = new GameObject("sceneContext");
                sceneContext.AddComponent<SceneContext>();
            }
            
            if (EditorSceneManager.SaveScene(scene, endPath))
            {
                Debug.Log($"Scene created {sceneName}");
                EditorSceneManager.CloseScene(scene, true);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();


                Debug.Log($"Add {sceneName} scene to EditorBuildSettings");
                var fullScenePath = Path.Combine("Assets", m_scenePath, sceneName + ".unity");
                EditorBuildSettingsScene buildScene = new EditorBuildSettingsScene(fullScenePath, true);
                EditorBuildSettings.scenes = EditorBuildSettings.scenes.Append(buildScene).ToArray();
            }
        }

        private GameObject AddCanvasComponents(string sceneName)
        {
            var root = new GameObject(sceneName);
            root.AddComponentWithPresets<Canvas>();
            root.AddComponentWithPresets<CanvasScaler>();
            root.AddComponentWithPresets<GraphicRaycaster>();
            return root;
        }

        private GameObject AddDummy(Transform root, string dummyName = "dummy")
        {
            var dummyGameObject = new GameObject(dummyName);
            dummyGameObject.transform.SetParent(root);
            var rectTransformDummy = dummyGameObject.AddComponent<RectTransform>();
            rectTransformDummy.anchorMin = Vector2.zero;
            rectTransformDummy.anchorMax = Vector2.one;
            rectTransformDummy.offsetMin = Vector2.zero;
            rectTransformDummy.offsetMax = Vector2.zero;
            return dummyGameObject;
        }

        private Type GetTypeByName(string clName)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.Name == clName)
                        return type;
                }
            }

            return null;
        }

        private string ClassNameScript => $"{m_className}Panel";

        [SerializeField] private string m_className = string.Empty;
        [SerializeField] private bool m_createSceneContext = true;
        [SerializeField] private bool m_createDummy = true;

        private const string ContinueOperationKey = "SceneCreator.ContinueOperationKey";
        private string m_scenePath = @"Scenes/UI";
        private string m_classPath = @"Scripts/UI/Panels";
    }
}