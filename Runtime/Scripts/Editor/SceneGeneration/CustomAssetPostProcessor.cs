using System.Linq;
using System.Threading.Tasks;
using UnityEditor;

namespace Playstrom.Core.Editor
{
    public class CustomAssetPostProcessor : AssetPostprocessor
    {
        static async void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            var needUpdate = importedAssets.Any(item => item.Contains(".unity")) ||
                             deletedAssets.Any(item => item.Contains(".unity")) ||
                             movedAssets.Any(item => item.Contains(".unity")) ||
                             movedFromAssetPaths.Any(item => item.Contains(".unity"));

            if (!needUpdate) return;
            
            await Task.Delay(1000);
            SceneCodeGeneration.GenerateGameScenesFile();
        }
    }
}