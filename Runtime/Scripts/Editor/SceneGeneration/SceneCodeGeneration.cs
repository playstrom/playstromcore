using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Playstrom.Core.Editor
{
    public class SceneCodeGeneration : EditorWindow
    {
        [InitializeOnLoadMethod]
        private static void OnProjectLoaded()
        {
            if (!m_initialised)
            {
                EditorBuildSettings.sceneListChanged += GenerateGameScenesFile;
                m_initialised = true;
            }
        }

        public static void GenerateGameScenesFile()
        {
            const string path = @"Scripts/Generated/GameScenes.cs";

            var fullPath = Path.Combine(Application.dataPath, path);
            var className = Path.GetFileNameWithoutExtension(fullPath);

            var sceneNames = GetSceneNamesFromBuildSettings();
            var code = GenerateClassWithConstants(className, sceneNames);
            CodeGenerationExtensions.WriteIntoFile(fullPath, code);
            Debug.Log("Refresh scenes in GameScenes");
            AssetDatabase.Refresh();
        }

        private static CodeCompileUnit GenerateClassWithConstants(
            string className,
            List<string> constants)
        {
            var compileUnit = new CodeCompileUnit();
            var @namespace = new CodeNamespace();
            var @class = new CodeTypeDeclaration(className);

            ImitateStaticClass(@class);

            for (var i = 0; i < constants.Count; i++)
            {
                var @const = GenerateConstant(constants[i]);
                @class.Members.Add(@const);
            }

            var field = new CodeMemberField()
            {
                Name = "cachedSceneNames",
                Attributes = MemberAttributes.Public | MemberAttributes.Static,
                Type = new CodeTypeReference(typeof(string[])),
                InitExpression = CodeDomUtility.Serialize(constants.ToArray())
            };

            @class.Members.Add(field);
            @namespace.Types.Add(@class);
            compileUnit.Namespaces.Add(@namespace);

            return compileUnit;
        }

        private static void ImitateStaticClass(CodeTypeDeclaration type)
        {
            type.TypeAttributes |= TypeAttributes.Sealed;
        }

        private static CodeMemberField GenerateConstant(string name)
        {
            name = name.Replace(" ", string.Empty);

            var @const = new CodeMemberField(
                typeof(string),
                name);

            @const.Attributes &= ~MemberAttributes.AccessMask;
            @const.Attributes &= ~MemberAttributes.ScopeMask;
            @const.Attributes |= MemberAttributes.Public;
            @const.Attributes |= MemberAttributes.Const;

            @const.InitExpression = new CodePrimitiveExpression(name);
            return @const;
        }

        private static List<string> GetSceneNamesFromBuildSettings()
        {
            var sceneNames = new List<string>();
            var scenePathFilter = "UI";
            var filteredScenes =
                EditorBuildSettings.scenes.Where(scene => File.Exists(scene.path) && scene.path.ToLower()
                        .Contains(scenePathFilter.ToLower())).ToList();

            for (var i = 0; i < filteredScenes.Count(); i++)
            {
                var sceneName = GetSceneNameFromPath(filteredScenes[i].path);
                sceneNames.Add(sceneName);
            }

            return sceneNames;
        }

        private static string GetSceneNameFromPath(string scenePath)
        {
            var sceneName = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath).name;
            return sceneName;
        }
        
        private static bool m_initialised;
    }
}