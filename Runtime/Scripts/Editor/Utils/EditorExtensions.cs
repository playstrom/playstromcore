using UnityEditor;
using UnityEngine;

namespace Core.playstromcore.Runtime.Scripts.Editor.Utils
{
    public static class EditorExtensions
    {
        public static void SetScriptingDefinedSymbols(string symbols)
        {
            var buildTargetGroup = GetActiveBuildTargetGroup();
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, symbols);
        }

        public static void AddScriptingDefinedSymbol(string symbol)
        {
            var buildTargetGroup = GetActiveBuildTargetGroup();
            var symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);
            if (symbols.Replace(";", " ").Contains(symbol))
            {
                Debug.LogWarning($"Scripting defined symbols already contains {symbol}");
                return;
            }
            var newSymbol = ";" + symbol;
            symbols = symbols.Insert(symbols.Length, newSymbol);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, symbols);
        }

        private static BuildTargetGroup GetActiveBuildTargetGroup()
        {
            var target = EditorUserBuildSettings.activeBuildTarget;
            var buildTargetGroup = BuildPipeline.GetBuildTargetGroup(target);
            return buildTargetGroup;
        }
    }
}