using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using Microsoft.CSharp;
using UnityEngine;

namespace Playstrom.Core.Editor
{
    public class CodeGenerationExtensions : MonoBehaviour
    {
        public static void WriteIntoFile(string fullPath, CodeCompileUnit code)
        {
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath) ?? string.Empty);
            }

            using (var stream = new StreamWriter(fullPath, false))
            {
                var writer = new IndentedTextWriter(stream);
                using (var codeProvider = new CSharpCodeProvider())
                {
                    codeProvider.GenerateCodeFromCompileUnit(code, writer, new CodeGeneratorOptions());
                }
            }
        }
        public static void WriteIntoFile(string fullPath, string code)
        {
            if (!Directory.Exists(fullPath))
            {
                Debug.Log($"Create directory {fullPath}");
                Directory.CreateDirectory(Path.GetDirectoryName(fullPath) ?? string.Empty);
            }

            using var stream = new StreamWriter(fullPath, false);
            stream.Write(code);
        }
        
        public static CodeTypeDeclaration GetEnum(string enumName, IEnumerable<string> enumValues)
        {
            var enumDeclaration = new CodeTypeDeclaration(enumName)
            {
                IsEnum = true
            };
            foreach (var valueName in enumValues)
            {
                var enumMember = new CodeMemberField(enumName, valueName);
                enumDeclaration.Members.Add(enumMember);
            }

            return enumDeclaration;
        }
    }
}