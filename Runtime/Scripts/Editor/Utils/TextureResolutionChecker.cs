using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UIElements.Image;

public class TextureResolutionChecker : EditorWindow
{
#if UNITY_2020_3_OR_NEWER
    [SerializeField] private int m_SelectedIndex = -1;
    
    private VisualElement m_RightPane;
    
    public void CreateGUI()
    {
        // Get a list of all sprites in the project
        var allObjectGuids = GetGuids();
        var allObjects = new List<Texture>();
        foreach (var guid in allObjectGuids)
        {
            allObjects.Add(AssetDatabase.LoadAssetAtPath<Texture>(AssetDatabase.GUIDToAssetPath(guid)));
        }

        // Create a two-pane view with the left pane being fixed with
        var splitView = new TwoPaneSplitView(0, 250, TwoPaneSplitViewOrientation.Horizontal);

        // Add the panel to the visual tree by adding it as a child to the root element
        rootVisualElement.Add(splitView);

        // A TwoPaneSplitView always needs exactly two child elements
        var leftPane = new ListView();

        splitView.Add(leftPane);
        m_RightPane = new ScrollView(ScrollViewMode.VerticalAndHorizontal);
        splitView.Add(m_RightPane);

        // Initialize the list view with all sprites' names
        leftPane.makeItem = () => new Label();
        leftPane.bindItem = (item, index) => { (item as Label).text = allObjects[index].name; };
        leftPane.itemsSource = allObjects;

        // React to the user's selection
        leftPane.onSelectionChange += OnSpriteSelectionChange;

        // Restore the selection index from before the hot reload
        leftPane.selectedIndex = m_SelectedIndex;

        // Store the selection index when the selection changes
        leftPane.onSelectionChange += (_) => { m_SelectedIndex = leftPane.selectedIndex; };
    }

    private void OnSpriteSelectionChange(IEnumerable<object> selectedItems)
    {
        // Clear all previous content from the pane
        m_RightPane.Clear();

        // Get the selected sprite
        var selectedSprite = selectedItems.First() as Texture2D;
        if (selectedSprite == null)
            return;
        Selection.activeObject = selectedSprite;
        // Add a new Image control and display the sprite
        var spriteImage = new Image
        {
            scaleMode = ScaleMode.ScaleToFit,
            style =
            {
                minWidth = 150,
                minHeight = 150,
                paddingTop = 20,
                paddingLeft = 20
            }
        };

#if UNITY_2021_3_OR_NEWER
        var sprite = Sprite.Create(selectedSprite, new Rect(0.0f, 0.0f, selectedSprite.width, selectedSprite.height),
        new Vector2(0.5f, 0.5f), 100.0f, 0, SpriteMeshType.FullRect);
        spriteImage.sprite = sprite;
#else
        spriteImage.image = selectedSprite;
#endif

        var pathText = AssetDatabase.GetAssetPath(selectedSprite);
        pathText = $"<b>Asset path</b> : {pathText}";
        var textComponent = new Label(pathText)
        {
            style =
            {
                paddingTop = 20,
                paddingLeft = 20
            }
        };
        
       
        var resolutionText = $"<b>Resolution :</b> : ({selectedSprite.width},{selectedSprite.height})";
        var resolutionTextComponent = new Label(resolutionText)
        {
            style =
            {
                paddingTop = 20,
                paddingLeft = 20
            }
        };

#if UNITY_2020
        ResizeForOldUnity(spriteImage);
#endif

        // Add the Image control to the right-hand pane
        m_RightPane.Add(textComponent);
        m_RightPane.Add(resolutionTextComponent);
        m_RightPane.Add(spriteImage);
    }

    private void ResizeForOldUnity(Image spriteImage)
    {
        var differenceHeight = spriteImage.image.height / (m_RightPane.layout.height - 90);
        var differenceWidth = spriteImage.image.width / m_RightPane.layout.width;
        if (spriteImage.image.height > m_RightPane.layout.height 
            && spriteImage.image.width > m_RightPane.layout.width)
        {
            if (differenceHeight > differenceWidth)
            {
                ResizeByHeight(spriteImage, differenceHeight);
            }
            else
            {
                ResizeByWidth(spriteImage, differenceWidth);
            }
        }
        else if (spriteImage.image.height > m_RightPane.layout.height)
        {
            ResizeByHeight(spriteImage, differenceHeight);
        }
        else if (spriteImage.image.width > m_RightPane.layout.width)
        {
            ResizeByWidth(spriteImage, differenceWidth);
        }
    }

    private static void ResizeByWidth(Image spriteImage, float differenceWidth)
    {
        spriteImage.style.height = spriteImage.image.height / differenceWidth;
        spriteImage.style.width = spriteImage.image.width / differenceWidth;
    }

    private static void ResizeByHeight(Image spriteImage, float differenceHeight)
    {
        spriteImage.style.height = spriteImage.image.height / differenceHeight;
        spriteImage.style.width = spriteImage.image.width / differenceHeight;
    }

    [MenuItem("Tools/Texture resolution check")]
    public static void TextureResolutionCheck()
    {
        var window = (TextureResolutionChecker)GetWindow(typeof(TextureResolutionChecker));
        window.Show();
        
        window.titleContent = new GUIContent("Texture Resolution Checker");

        // Limit size of the window
        window.minSize = new Vector2(450, 200);
        window.maxSize = new Vector2(1920, 720);
    }

    private List<string> GetGuids()
    {
        var guids = AssetDatabase.FindAssets("t:texture2D");
        List<string> toRemove = new List<string>();
        foreach (var guid in guids)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            if (!path.StartsWith("Assets")) //TODO Rework with editable excludes
            {
                continue;
            }
            var asset = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));

            if (string.IsNullOrEmpty(path) || asset == null)
            {
                continue;
            }

            if (!IsPowerOfTwo(asset.width) || !IsPowerOfTwo(asset.height))
            {
                toRemove.Add(guid);
                continue;
            }
        }

        return toRemove;
    }

    private static bool IsPowerOfTwo(int n)
    {
        if (n == 0)
            return false;

        while (n != 1)
        {
            if (n % 2 != 0)
                return false;

            n = n / 2;
        }

        return true;
    }
#endif
}