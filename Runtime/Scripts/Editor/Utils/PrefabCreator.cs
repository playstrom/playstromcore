using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Playstrom.Core.Editor
{
    public static class PrefabCreator
    {
        /// <summary>
        /// Method for creating prefabs to Application.DataPath/Prefabs
        /// </summary>
        /// <param name="prefabName"></param>
        /// <param name="path"> path after /Prefabs</param>
        /// <returns></returns>
        public static GameObject CreatePrefab(string prefabName, string path)
        {
            return CreatePrefab(PrefabPath, prefabName, path);
        }

        /// <summary>
        /// Method for creating prefabs to Application.DataPath/Resources
        /// </summary>
        /// <param name="prefabName"></param>
        /// <param name="path"> path after /Prefabs</param>
        /// <returns></returns>
        public static GameObject CreatePrefabToResources(string prefabName, string path)
        {
            return CreatePrefab(ResourcesPath, prefabName, path);
        }

        private static GameObject CreatePrefab(string folderPath, string prefabName, string path)
        {
            var directory = $"{Application.dataPath}{folderPath}{path}";
            if (!Directory.Exists(directory))
            {
                Debug.Log($"Create directory {directory}");
                Directory.CreateDirectory(directory);
                AssetDatabase.Refresh();
            }

            var prefabPath = $"{directory}/{prefabName}.prefab";
            if (File.Exists(prefabPath))
            {
                Debug.LogWarning($"File exists on {prefabPath}");
                return AssetDatabase.LoadAssetAtPath<GameObject>($"Assets{folderPath}{path}/{prefabName}.prefab");
            }

            var sceneGameObject = new GameObject($"{prefabName}");
            var prefab = PrefabUtility.SaveAsPrefabAssetAndConnect(sceneGameObject, prefabPath, InteractionMode.UserAction);

            AssetDatabase.Refresh();
            Object.DestroyImmediate(sceneGameObject);

            return prefab;
        }
        
        private static string PrefabPath => $"/Prefabs";
        private static string ResourcesPath => $"/Resources";
    }
}