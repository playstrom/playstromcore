using Playstrom.Core.Ui;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Playstrom.Core
{
    public class MenuItems : MonoBehaviour
    {
        [MenuItem("GameObject/UI/PlayStrom/Buttons/Ads Button")]
        private static void CreateAdsButton()
        {
            var buttonGameObject = new GameObject("AdsButton");

            buttonGameObject.AddComponentWithPresets<Image>();
            buttonGameObject.AddComponentWithPresets<AdsButton>();

            buttonGameObject.transform.SetParentAndSetDefault(Selection.activeTransform);

            Selection.activeGameObject = buttonGameObject;
        }

        [MenuItem("GameObject/UI/PlayStrom/Buttons/Settings Button")]
        private static void CreateSettingsButton()
        {
            var buttonGameObject = new GameObject("SettingsButton");

            buttonGameObject.AddComponentWithPresets<Image>();
            buttonGameObject.AddComponentWithPresets<SettingsButton>();

            buttonGameObject.transform.SetParentAndSetDefault(Selection.activeTransform);

            Selection.activeGameObject = buttonGameObject;
        }

        [MenuItem("GameObject/UI/PlayStrom/Buttons/Simple Button")]
        private static void CreateSimpleButton()
        {
            var buttonGameObject = new GameObject("SimpleButton");

            buttonGameObject.AddComponentWithPresets<Image>();
            buttonGameObject.AddComponentWithPresets<SettingsButton>();

            buttonGameObject.transform.SetParentAndSetDefault(Selection.activeTransform);

            Selection.activeGameObject = buttonGameObject;
        }
    }
}