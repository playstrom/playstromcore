using System;
using System.Collections;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Object = UnityEngine.Object;

namespace Playstrom.Core.Editor
{
    public class FoldersSwitcher : EditorWindow
    {
        [MenuItem("Tools/Playstrom/UI Folder Switcher")]
        private static void Init()
        {
            var wnd = GetWindow<FoldersSwitcher>();
            wnd.titleContent = new GUIContent("Playstrom UI utilities");
        }

        private Button _scriptsUiButton;
        private Button _prefabsButton;
        private Button _sceneButton;

        private void CreateGUI()
        {
            var root = rootVisualElement;
            _scriptsUiButton = CreateButton("Open Scripts/UI", OpenScriptsUi);
            _prefabsButton = CreateButton("Open Prefabs/UI", OpenPrefabs);
            _sceneButton = CreateButton("Open Scenes/UI", OpenScene);


            root.Add(_scriptsUiButton);
            root.Add(_prefabsButton);
            root.Add(_sceneButton);
            root.Bind(new SerializedObject(this));
        }

        private Button CreateButton(string name, Action onClick)
        {
            var button = new Button(onClick)
            {
                text = name,
            };
            return button;
        }


        private void OpenScene()
        {
            SelectFolder("Scenes/UI");
        }

        private void OpenPrefabs()
        {
            SelectFolder("Prefabs/UI");
        }

        private void OpenScriptsUi()
        {
            SelectFolder("Scripts/UI");
        }

        private static void SelectFolder(string assetPath)
        {
            var folder = AssetDatabase.LoadAssetAtPath<Object>($"Assets/{assetPath}");
            if (folder != null)
            {
                var firstFilePath = GetFirstFileAtPath(assetPath);
                var pathFirstFilePath = firstFilePath.Split("/").LastOrDefault();
                
                Selection.activeObject = folder;
            }
            else
            {
                Ui.Logger.Warning($"No exist folder {assetPath}");
            }
        }

        private static string GetFirstFileAtPath(string path)
        {
            var fileEntries = Directory.GetFiles(Application.dataPath + "/" + path);
            if (fileEntries.Length > 0)
            {
                return fileEntries[0];
            }

            return string.Empty;
        }
    }
}