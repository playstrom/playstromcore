#if UNITY_EDITOR
using System;
using Newtonsoft.Json;
using Playstrom.Core.Ui;
using UnityEngine;

namespace UI
{
    /// <summary>
    /// Base Template for control data that required to be serialized, such as level, score, settings, etc.
    /// 
    /// </summary>
    public static class PlayerDataTemplate
    {
        /// <summary>
        /// This method has been called automatically when the application has started
        /// </summary>
        [RuntimeInitializeOnLoadMethod]
        private static void Init()
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };


            playerInfo = Data.Exist(PlayerInfoKey) ? Data.Get<PlayerInfo>(PlayerInfoKey) : new PlayerInfo();

            Save();
        }

        private static void Save()
        {
            Data.SaveToFile(playerInfo, PlayerInfoKey);
        }

        [Serializable]
        public class PlayerInfo
        {
            public PlayerInfo()
            {
                m_sound = true;
                m_music = true;
            }

            public bool m_sound;
            public bool m_music;
        }

        private static PlayerInfo playerInfo;
        private const string PlayerInfoKey = "Player";
    }
}

#endif