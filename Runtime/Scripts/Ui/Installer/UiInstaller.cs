using Playstrom.Core.Ui;
using Zenject;

public class UiInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<UiManager>().AsCached().NonLazy();
    }
}