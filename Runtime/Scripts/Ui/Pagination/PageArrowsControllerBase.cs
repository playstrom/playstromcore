using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Pagination
{
    [RequireComponent(typeof(Pagination))]
    public class PageArrowsControllerBase : MonoBehaviour
    {
        public virtual void CheckPageButtons()
        {
            if (Pagination.MaxPageIndex == 0)
            {
                _prevButton.gameObject.SetActive(false);
                _nextButton.gameObject.SetActive(false);
            }
            else if (Pagination.CurrentPage == 0)
            {
                _prevButton.gameObject.SetActive(false);
                _nextButton.gameObject.SetActive(true);
            }
            else if (Pagination.CurrentPage == Pagination.MaxPageIndex)
            {
                _prevButton.gameObject.SetActive(true);
                _nextButton.gameObject.SetActive(false);
            }
            else
            {
                _prevButton.gameObject.SetActive(true);
                _nextButton.gameObject.SetActive(true);
            }
        }

        protected virtual void Awake()
        {
            Pagination = GetComponent<Pagination>();
        }

        protected virtual void Start()
        {
            _prevButton.onClick.AddListener(SelectPreviousPage);
            _nextButton.onClick.AddListener(SelectNextPage);
        }

        protected void OnDestroy()
        {
            _prevButton.onClick.RemoveAllListeners();
            _nextButton.onClick.RemoveAllListeners();
        }

        protected virtual void SelectPreviousPage()
        {
            Pagination.SetPreviousPage();
            CheckPageButtons();
        }

        protected virtual void SelectNextPage()
        {
            Pagination.SetNextPage();
            CheckPageButtons();
        }

        [Header("Button events initialisation mode is in Start()")] [SerializeField]
        private Button _prevButton;

        [SerializeField] private Button _nextButton;

        protected Pagination Pagination;
    }
}