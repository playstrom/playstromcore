using UnityEngine;

namespace UI.Pagination
{
    public abstract class PageIconBase : MonoBehaviour
    {
        public int PageIndex { get; private set; }
        public abstract void SetSelected(bool selected, object data);

        public virtual void Init(int index)
        {
            PageIndex = index;
        }
    }
}