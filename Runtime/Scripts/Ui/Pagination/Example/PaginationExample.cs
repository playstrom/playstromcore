using UnityEngine;

namespace UI.Pagination.Example
{
    public class PaginationExample : MonoBehaviour
    {
        [SerializeField] private Pagination _pagination;

        [SerializeField] private int _itemsOnPage = 6;
        [SerializeField] private int _elementsAmount = 31;

        private void Start()
        {
            _pagination.Init(_itemsOnPage, _elementsAmount, UpdateElements);
            _pagination.InitialisePages();
        }

        private void UpdateElements()
        {
            Debug.Log($"current page is{_pagination.CurrentPage}");
        }
    }
}