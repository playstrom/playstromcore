using UnityEngine;

namespace UI.Pagination.Example
{
    public class PageIconsController : PageIconsControllerBase
    {
        [SerializeField] private Color _activeColor;
        [SerializeField] private Color _nonActiveColor;

        protected override object GetDataToUpdatePages(bool selected)
        {
            return selected ? _activeColor : _nonActiveColor;
        }
    }
}