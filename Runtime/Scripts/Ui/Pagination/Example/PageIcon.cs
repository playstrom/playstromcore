using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Pagination.Example
{
    public class PageIcon : PageIconBase
    {
        [SerializeField] private Image _icon;

        public override void SetSelected(bool selected, object data)
        {
            var color = (Color)data;
            _icon.DOColor(color, .2f);
        }
    }
}