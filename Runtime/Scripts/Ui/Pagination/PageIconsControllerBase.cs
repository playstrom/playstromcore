using System.Collections.Generic;
using Playstrom.Core.Extensions;
using UnityEngine;

namespace UI.Pagination
{
    [RequireComponent(typeof(Pagination))]
    public abstract class PageIconsControllerBase : MonoBehaviour
    {
        public virtual void Init()
        {
            _root.Clear();
            PageIcons.Clear();
            if (_showPageIconIfSinglePage == false)
            {
                if (_pagination.CurrentPage == 1)
                {
                    return;
                }

                for (var i = 0; i <= _pagination.MaxPageIndex; i++)
                {
                    var rootIcon = CreatePagePrefab();
                    rootIcon.Init(i);
                    PageIcons.Add(rootIcon);
                }

                UpdatePages(_pagination.CurrentPage);
            }
        }

        public virtual void UpdatePages(int currentPage)
        {
            foreach (var item in PageIcons)
            {
                var selected = item.PageIndex == currentPage;
                var data = GetDataToUpdatePages(selected);
                item.SetSelected(selected, data);
            }
        }

        /// <summary>
        /// You can use instantiate or Pool
        /// </summary>
        public virtual PageIconBase CreatePagePrefab()
        {
            return Instantiate(_pageIconPrefab, _root);
        }

        protected abstract object GetDataToUpdatePages(bool selected);

        private void Awake()
        {
            _pagination = GetComponent<Pagination>();
        }

        [SerializeField] private PageIconBase _pageIconPrefab;
        [SerializeField] private RectTransform _root;
        [SerializeField] private bool _showPageIconIfSinglePage;

        protected readonly List<PageIconBase> PageIcons = new();

        private Pagination _pagination;
    }
}