using System;
using UnityEngine;

namespace UI.Pagination
{
    public sealed class Pagination : MonoBehaviour
    {
        public int MaxPageIndex { get; private set; }

        public int CurrentPage
        {
            get => _currentPage;
            set
            {
                _currentPage = value;
                _pageIconsController.UpdatePages(_currentPage);
            }
        }

        public void Init(int itemsOnPage, int elementsAmount, Action onPageChanged)
        {
            _itemsOnPage = itemsOnPage;
            _elementsAmount = elementsAmount;
            _onPageChanged = onPageChanged;
        }

        public void SetNextPage()
        {
            if (CurrentPage < MaxPageIndex)
            {
                CurrentPage++;
                _onPageChanged?.Invoke();
            }
        }

        public void SetPreviousPage()
        {
            if (CurrentPage > 0)
            {
                CurrentPage--;
                _onPageChanged?.Invoke();
            }
        }

        public int GetPage(int index)
        {
            return index / _itemsOnPage;
        }

        public void InitialisePages()
        {
            MaxPageIndex = _elementsAmount / _itemsOnPage;
            if (_elementsAmount % _itemsOnPage == 0)
            {
                MaxPageIndex--;
            }

            MaxPageIndex = Mathf.Clamp(MaxPageIndex, 0, int.MaxValue);

            _pageIconsController.Init();
            _pageArrowsControllerBase.CheckPageButtons();
        }

        [SerializeField] private PageIconsControllerBase _pageIconsController;
        [SerializeField] private PageArrowsControllerBase _pageArrowsControllerBase;

        private Action _onPageChanged;

        private int _itemsOnPage;
        private int _elementsAmount;
        private int _currentPage;
    }
}