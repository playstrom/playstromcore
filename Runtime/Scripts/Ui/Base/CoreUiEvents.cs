using System;

namespace Playstrom.Core.Ui
{
    public static class CoreUiEvents
    {
        public static event Action<int, bool> OnSettingsChanged;
        public static void ChangeSettingsState(int setting, bool state)
        {
            OnSettingsChanged?.Invoke(setting, state);
        }
    }
}