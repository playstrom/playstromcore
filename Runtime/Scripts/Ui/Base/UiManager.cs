﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Playstrom.Core.Ui
{
    public class UiManager
    {
        public event Action<string> OnSceneLoaded;
        public event Action<string> OnSceneUnloaded;

        public virtual async void ShowUiPanel(string panelName, bool unloadCurrentOtherPanels = true)
        {
            if (panelName == PanelName)
            {
                return;
            }

            if (m_panelsToHide.TryGetValue(panelName, out _))
            {
                return;
            }

            UiPanelBase panel = FindInitialisedUiPanel(panelName);
            if (unloadCurrentOtherPanels)
            {
                foreach (var loadingPanelType in m_loadingUi)
                {
                    m_targetUnloadPanels.Add(loadingPanelType);
                }

                foreach (var initedPanel in m_initedPanels)
                {
                    m_targetUnloadPanels.Add(initedPanel.Key);
                }
            }

            m_targetUnloadPanels.Remove(panelName);
            if (panel != null)
            {
                ShowUiPanelInternal(panel, unloadCurrentOtherPanels);
                return;
            }

            if (!IsLoading(panelName))
            {
                InitPanel(panelName);
            }

            while (IsLoading(panelName))
            {
                await Task.Delay(10);
            }

            panel = FindInitialisedUiPanel(panelName);
            ShowUiPanelInternal(panel, unloadCurrentOtherPanels);
        }

        public virtual void HidePanel(string panelNameToHide)
        {
            if (m_panelsToHide.TryGetValue(panelNameToHide, out _))
            {
                return;
            }

            if (!m_showedPanels.TryGetValue(panelNameToHide, out var panel))
            {
                return;
            }


            panel.Hide(TryDeinitAsync);
            m_panelsToHide.TryAdd(panelNameToHide, panel);
            m_showedPanels.Remove(panelNameToHide);
            if (PanelName == panelNameToHide)
            {
                PanelName = string.Empty;
            }
        }

        public virtual void ReloadPanel(string panelName, bool unloadCurrentOtherPanels = true)
        {
            if (!m_showedPanels.TryGetValue(panelName, out var panel))
            {
                ShowUiPanel(panelName, unloadCurrentOtherPanels);
                return;
            }

            panel.Hide();
            DeInitAsyncBasePanel(panelName, OnPanelUnloaded);

            void OnPanelUnloaded(string unloadedPanelName)
            {
                ShowUiPanel(unloadedPanelName, unloadCurrentOtherPanels);
            }

            if (PanelName == panelName)
            {
                PanelName = string.Empty;
            }
        }

        public virtual void InitPanel(string panelName)
        {
            if (IsInitialised(panelName))
            {
                return;
            }

            if (IsLoading(panelName))
            {
                return;
            }

            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(panelName, LoadSceneMode.Additive);
            var item = new AsyncLoadingSceneUi(asyncOperation, panelName);
            m_loadingUi.Add(panelName);
            asyncOperation.completed += OnAsyncOperationCompleted;

            void OnAsyncOperationCompleted(AsyncOperation operation)
            {
                asyncOperation.completed -= OnAsyncOperationCompleted;
                OnUiPanelLoaded(item);
            }
        }

        public void DeInitAsyncBasePanel(string panelName, Action<string> callback = null)
        {
            UiPanelBase panel = FindInitialisedUiPanel(panelName);
            panel.DeInit();
            var unloadAsyncOp = SceneManager.UnloadSceneAsync(panelName);
            unloadAsyncOp.completed += (op) =>
            {
                m_initedPanels.Remove(panelName);
                m_showedPanels.Remove(panelName);
                m_panelsToHide.Remove(panelName);
                callback?.Invoke(panelName);
                OnSceneUnloaded?.Invoke(panelName);
            };
        }

        private void ShowUiPanelInternal(UiPanelBase activate, bool unloadCurrentOtherPanels = true)
        {
            if (m_showedPanels.ContainsKey(activate.Scene))
            {
#if UNITY_EDITOR
                Debug.LogWarning("Trying to add the scene that already exist");
                return;
#endif
            }

            if (unloadCurrentOtherPanels)
            {
                var targetHidePanels = m_showedPanels.Keys.Where(m_targetUnloadPanels.Contains).ToList();
                foreach (string targetHidePanel in targetHidePanels)
                {
                    m_targetUnloadPanels.Remove(targetHidePanel);
                    HidePanel(targetHidePanel);
                }
            }

            foreach (var kvp in m_showedPanels)
            {
                kvp.Value.ApplyOverlay(activate.Scene);
            }

            activate.Show();
            activate.ApplyOverlay(PanelName);

            m_showedPanels.TryAdd(activate.Scene, activate);
            PanelName = activate.Scene;
        }

        private void TryDeinitAsync(UiPanelBase showedPanel)
        {
            if (!showedPanel.IsNotDeInitAsync)
            {
                DeInitAsyncBasePanel(showedPanel.Scene);
            }
        }

        private void OnUiPanelLoaded(AsyncLoadingSceneUi item)
        {
            var scene = SceneManager.GetSceneByName(item.UiPanelType);
            UiPanelBase foundedPanel = null;
            foreach (GameObject rootGameObject in scene.GetRootGameObjects())
            {
                foundedPanel = rootGameObject.GetComponent<UiPanelBase>();
                if (foundedPanel != null)
                {
                    break;
                }
            }

            if (foundedPanel == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Can't find UiPanelBase in scene " + item.UiPanelType);
#endif
                return;
            }

            foundedPanel.Init();
            m_initedPanels.TryAdd(item.UiPanelType, foundedPanel);
            m_loadingUi.Remove(item.UiPanelType);
            if (m_targetUnloadPanels.Remove(item.UiPanelType))
            {
                TryDeinitAsync(foundedPanel);
            }

            OnSceneLoaded?.Invoke(item.UiPanelType);
        }


        private bool IsInitialised(string panelName)
        {
            return m_initedPanels.ContainsKey(panelName);
        }

        private UiPanelBase FindInitialisedUiPanel(string panelName)
        {
            m_initedPanels.TryGetValue(panelName, out var res);
            return res;
        }

        private bool IsShowed(string panelName)
        {
            return m_showedPanels.ContainsKey(panelName);
        }

        private bool IsLoading(string panelName)
        {
            return m_loadingUi.Contains(panelName);
        }


        [Serializable]
        public class AsyncLoadingSceneUi
        {
            public AsyncOperation AsyncOperation { get; private set; }
            public string UiPanelType { get; private set; }

            public AsyncLoadingSceneUi(AsyncOperation operation, string panelType)
            {
                AsyncOperation = operation;
                UiPanelType = panelType;
            }
        }

        protected Dictionary<string, UiPanelBase> m_showedPanels = new Dictionary<string, UiPanelBase>();
        protected Dictionary<string, UiPanelBase> m_panelsToHide = new Dictionary<string, UiPanelBase>();
        protected Dictionary<string, UiPanelBase> m_initedPanels = new Dictionary<string, UiPanelBase>();

        /// <summary>
        /// This panelTypes under loading now
        /// </summary>
        private HashSet<string> m_loadingUi = new HashSet<string>();

        private HashSet<string> m_targetUnloadPanels = new HashSet<string>();

        public string PanelName { get; private set; }
    }
}