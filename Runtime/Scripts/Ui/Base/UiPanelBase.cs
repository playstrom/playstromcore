﻿using System;
using UnityEngine;
using Zenject;

namespace Playstrom.Core.Ui
{
    public class UiPanelBase : MonoBehaviour
    {
        public string Scene => gameObject.scene.name;
        public bool IsNotDeInitAsync => m_isNotDeInitAsync;

        public virtual void Init()
        {
            Canvas canvas = GetComponent<Canvas>();
            if (canvas == null)
            {
                return;
            }

            canvas.worldCamera = Camera.main;

            if (m_dummy == null)
            {
                return;
            }

            m_canvasGroup = m_dummy.GetComponent<CanvasGroup>();
        }

        public virtual void DeInit()
        {
        }

        public virtual void Reset()
        {
        }

        public virtual void ApplyOverlay(string panelType)
        {
        }

        public virtual void Show()
        {
            if (m_dummy != null)
            {
                m_dummy.SetActive(true);
            }

            UiAnimEventHandler.Invoke(m_showHide, AnimType.Show);
        }

        public virtual void Hide(Action<UiPanelBase> onCompleted = null)
        {
            UiAnimEventHandler.Invoke(m_showHide, AnimType.Hide,
                () =>
                {
                    m_dummy.SetActive(false);
                    onCompleted?.Invoke(this);
                });
        }

        public virtual bool IsInAnimation()
        {
            return false;
        }

        protected T GetParam<T>(string key) where T : class
        {
            var container = ProjectContext.Instance.Container;
            var containerValue = container.TryResolveId<T>(key);
            if (containerValue == null)
            {
                Logger.Error($"No exist parameter in container with key {key}");
                return default;
            }

            return containerValue;
        }
        
        #if UNITY_EDITOR

        public void SetDummy(GameObject dummy)
        {
            m_dummy = dummy;
        }
        
        #endif

        [SerializeField] private bool m_isNotDeInitAsync;
        [SerializeField] private GameObject m_dummy;
        [SerializeField] private UnityEventAnimType m_showHide = new UnityEventAnimType();

        private CanvasGroup m_canvasGroup;
    }
}