﻿using System;

namespace Playstrom.Core.Ui
{
    public interface IButtonStatus
    {
        bool IsAllow(string key);
    }

    public class NormalButtonStatus : IButtonStatus
    {
        public bool IsAllow(string key)
        {
            return true;
        }
    }
}