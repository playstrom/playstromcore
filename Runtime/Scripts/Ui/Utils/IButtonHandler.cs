﻿using System;

namespace Playstrom.Core.Ui
{
    public interface IButtonHandler
    {
        void Handle(Action action, object args);
    }

    public class ButtonHandler : IButtonHandler
    {
        public void Handle(Action action, object args)
        {
            action?.Invoke();
        }
    }
}