﻿using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Playstrom.Core.Ui
{
    //TODO Как помне то слишком много логики для одного ButtonController
    //Из предложений сделать все таки базовый и наследников от него, мы так хоть немного к SRP двигаться будем
    public class ButtonController : Button
    {
        public bool IsOn
        {
            get => m_isOn;
            set
            {
                m_isOn = value;
                if (m_buttonType == ButtonType.Setting)
                    SetOnOffState(m_isOn);
            }
        }

        public void SetVisible(bool value, bool isNeedAnimate = false)
        {
            if (value)
            {
                gameObject.SetActive(true);
                if (isNeedAnimate)
                {
                    UiAnimEventHandler.Invoke(m_showHide, AnimType.Show);
                }
            }
            else
            {
                if (isNeedAnimate)
                {
                    UiAnimEventHandler.Invoke(m_showHide, AnimType.Hide,
                        () => { gameObject.SetActive(false); });
                }
                else gameObject.SetActive(false);
            }
        }

        public void SetButtonLogic(Action action, IButtonHandler buttonHandler = null, string buttonName = null,
            IButtonStatus buttonStatus = null)
        {
            m_buttonName = buttonName;
            buttonHandler ??= new ButtonHandler();
            buttonStatus ??= new NormalButtonStatus();
            onClick.RemoveAllListeners();
            onClick.AddListener(() => buttonHandler.Handle(action, m_buttonName));
            m_buttonStatus = buttonStatus;
        }

        public void DisableAds()
        {
            m_adsIcon.SetActive(false);
            m_adsDownload.SetActive(false);
            m_buttonType = ButtonType.Simple;
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            base.DoStateTransition(state, instant);
            switch (state)
            {
                case SelectionState.Normal:
                case SelectionState.Selected:
                    if (m_currentState == SelectionState.Normal) break;
                    UiAnimEventHandler.Invoke(m_changeButtonState, AnimType.Normal);
                    m_currentState = SelectionState.Normal;
                    break;
                case SelectionState.Pressed:
                    UiAnimEventHandler.Invoke(m_changeButtonState, AnimType.Pressed);
                    m_currentState = SelectionState.Pressed;
                    break;
                case SelectionState.Disabled:
                    UiAnimEventHandler.Invoke(m_changeButtonState, AnimType.Disable);
                    m_currentState = SelectionState.Disabled;
                    break;
                case SelectionState.Highlighted:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        private void SetOnOffState(bool isOn)
        {
            if (m_onStateSprite == null || m_offStateSprite == null)
            {
#if UNITY_EDITOR
                Debug.LogWarning(
                    $"No available one of state sprites OnStateSprite {m_onStateSprite}, OffStateSprite {m_offStateSprite}");
#endif
                return;
            }

            image.sprite = isOn ? m_onStateSprite : m_offStateSprite;
        }

        private IEnumerator CheckAdsAvailable()
        {
            const float checkDelay = 1f;

            SetAdsLoadingState(false);

            while (true)
            {
                if (Network.IsNetworkAvailable == false || m_buttonStatus.IsAllow(m_buttonName) == false)
                {
                    SetAdsLoadingState(false);
                    yield return new WaitForSeconds(checkDelay);
                    continue;
                }

                SetAdsLoadingState(true);
                yield return new WaitForSeconds(checkDelay);
            }
        }

        private void SetAdsLoadingState(bool isAdsAvailable)
        {
            if (m_adsIcon == null && m_adsDownload == null) return;

            if (m_adsIcon != null) m_adsIcon.SetActive(isAdsAvailable);
            m_adsDownload.SetActive(!isAdsAvailable);
            UiAnimEventHandler.Invoke(m_changeAdsState,
                isAdsAvailable ? AnimType.AdsAvailable : AnimType.AdsNotAvailable);
        }

        private void InitButton()
        {
            switch (m_buttonType)
            {
                case ButtonType.Simple:
                    break;
                case ButtonType.Setting:
                    break;
                case ButtonType.Ads:
                    Network.CheckConnection();
                    StartCoroutine(CheckAdsAvailable());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected override void Start()
        {
            base.Start();
#if !UNITY_EDITOR
            InitButton();
#endif
        }

        [SerializeField] private UnityEventAnimType m_changeButtonState;
        [SerializeField] private UnityEventAnimType m_showHide;

        [SerializeField] private ButtonType m_buttonType = ButtonType.Simple;

        [SerializeField] private Sprite m_onStateSprite;
        [SerializeField] private Sprite m_offStateSprite;

        [SerializeField] private GameObject m_adsIcon;
        [SerializeField] private GameObject m_adsDownload;

        [SerializeField] private UnityEventAnimType m_changeAdsState;

        private bool m_isOn = true;
        private string m_buttonName = string.Empty;
        private SelectionState m_currentState = SelectionState.Normal;
        private IButtonStatus m_buttonStatus;


        private enum ButtonType
        {
            Simple,
            Setting,
            Ads
        }
    }
}