﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Playstrom.Core.Ui
{
    public class UiTweener : MonoBehaviour
    {
        public enum TweenAnimType
        {
            Rotate,
            Scale,
            MoveToAnchoredPosition,
            Color,
            CanvasGroup,
        }

        [SerializeField] private AnimType m_animType = AnimType.None;
        [SerializeField] private bool m_playOnAwake = false;
        [SerializeField] private List<UiAnimTweenObj> m_animTweenObjs = new List<UiAnimTweenObj>();

        private Dictionary<Tween, TweenAnimType> m_tweensMap = new Dictionary<Tween, TweenAnimType>();
        private bool m_isPlaying = false;

        public bool IsPlaying => m_isPlaying;
        public AnimType AnimType => m_animType;
        
        private void Awake()
        {
            if (m_playOnAwake) Play();
        }

        private void OnTweenerComplete(Tween tweener)
        {
            m_tweensMap.Remove(tweener);
            m_isPlaying = m_tweensMap.Any(e => e.Key.IsPlaying());
        }

        private void KillTween(IEnumerable<Tween> tweens)
        {
            foreach (var tw in tweens)
            {
                tw.Kill();
                m_tweensMap.Remove(tw);
            }
        }

        private List<Tween> GetAllTweensInObject(UiAnimTweenObj tween)
        {
            var tweensRect = DOTween.TweensByTarget(tween.Rect);
            var tweensGraphic = DOTween.TweensByTarget(tween.Rect);
            var tweensCanvas = DOTween.TweensByTarget(tween.Rect);

            var allTypes = new[] {tweensRect, tweensGraphic, tweensCanvas}.Where(x => x != null).SelectMany(x => x)
                .ToList();
            return allTypes;
        }

        private void ApplyTweenAnim(UiAnimTweenObj tween)
        {
            List<Tween> tweens = GetAllTweensInObject(tween);
            if (tweens != null)
            {
                var forKill = tweens.Where(m_tweensMap.ContainsKey)
                    .Where(t => m_tweensMap[t] == tween.TweenAnimType);
                KillTween(forKill);
            }

            Tween tweener = null;
            switch (tween.TweenAnimType)
            {
                case TweenAnimType.Rotate:
                    tween.Rect.localRotation = Quaternion.Euler(tween.StartValue);
                    tweener = tween.Rect.DOLocalRotate(tween.EndValue, tween.Time, RotateMode.FastBeyond360);
                    tweener.SetEase(tween.AnimCurveType).SetLoops(tween.NeedLoop ? -1 : 1, tween.LoopTweenType)
                        .OnComplete(() => OnTweenerComplete(tweener));
                    break;
                case TweenAnimType.Scale:
                    tween.Rect.localScale = tween.StartValue;
                    tweener = tween.Rect.DOScale(tween.EndValue, tween.Time);
                    tweener.SetEase(tween.AnimCurveType).SetLoops(tween.NeedLoop ? -1 : 1, tween.LoopTweenType)
                        .OnComplete(() => OnTweenerComplete(tweener));
                    break;
                case TweenAnimType.MoveToAnchoredPosition:
                    tween.Rect.anchoredPosition = tween.StartValue;
                    tweener = tween.Rect.DOAnchorPos(tween.EndValue, tween.Time);
                    tweener.SetEase(tween.AnimCurveType).SetLoops(tween.NeedLoop ? -1 : 1, tween.LoopTweenType)
                        .OnComplete(() => OnTweenerComplete(tweener));
                    break;
                case TweenAnimType.Color:
                    var graphic = tween.Rect.GetComponent<Graphic>();
                    if (graphic == null) return;
                    graphic.color = tween.StartColor;
                    tweener = graphic.DOColor(tween.EndColor, tween.Time);
                    tweener.SetEase(tween.AnimCurveType).SetLoops(tween.NeedLoop ? -1 : 1, tween.LoopTweenType)
                        .OnComplete(() => OnTweenerComplete(tweener));
                    break;
                case TweenAnimType.CanvasGroup:
                    var canvas = tween.Rect.GetComponent<CanvasGroup>();
                    if (canvas == null) return;
                    canvas.alpha = tween.StartAlpha;
                    if (tween.ChangeInteractable && (tween.StartAlpha < 1 || tween.EndAlpha < 1))
                    {
                        canvas.interactable = false;
                        canvas.blocksRaycasts = false;
                    }
                    tweener = canvas.DOFade(tween.EndAlpha, tween.Time);
                    tweener.SetEase(tween.AnimCurveType).SetLoops(tween.NeedLoop ? -1 : 1, tween.LoopTweenType)
                        .OnComplete(() =>
                        {
                            if (tween.ChangeInteractable && (Math.Abs(tween.EndAlpha - 1) < 0.1f))
                            {
                                canvas.interactable = true;
                                canvas.blocksRaycasts = true;
                            }

                            OnTweenerComplete(tweener);
                        });
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (tweener != null) m_tweensMap.Add(tweener, tween.TweenAnimType);
        }

        public float GetLongestTime()
        {
            return m_animTweenObjs.Max(ato => ato.Time);
        }
        
        public void Play()
        {
            foreach (UiAnimTweenObj tween in m_animTweenObjs)
            {
                ApplyTweenAnim(tween);
            }

            m_isPlaying = true;
        }

        // protected void PlayByIndex(int index)
        // {
        //     if (m_animTweenObjs[index] == null) return;
        //     ApplyTweenAnim(m_animTweenObjs[index]);
        // }

        // protected void PlayRandom()
        // {
        //     var index = Random.Range(0, m_animTweenObjs.Count - 1);
        //     PlayByIndex(index);
        //     if (m_animTweenObjs[index] == null) return;
        //     ApplyTweenAnim(m_animTweenObjs[index]);
        // }
        //
        // protected void PlayRandomRange(int index1, int index2)
        // {
        //     var index = Random.Range(index1, index2);
        //     PlayByIndex(index);
        //     if (m_animTweenObjs[index] == null) return;
        //     ApplyTweenAnim(m_animTweenObjs[index]);
        // }

        public void Stop()
        {
            foreach (UiAnimTweenObj tween in m_animTweenObjs)
            {
                var tweens = GetAllTweensInObject(tween);
                if (tweens == null) return;
                var forKill = tweens.Where(m_tweensMap.ContainsKey);
                KillTween(forKill);
            }

            m_isPlaying = false;
        }

        // protected void StopByIndex(int index)
        // {
        //     if (m_animTweenObjs[index] == null) return;
        //     UiAnimTweenObj tween = m_animTweenObjs[index];
        //     if (tween == null) return;
        //
        //     List<Tween> tweens = DOTween.TweensByTarget(tween.Rect);
        //     if (tweens == null) return;
        //     var forKill = tweens.Where(m_tweensMap.ContainsKey);
        //     KillTween(forKill);
        // }

        public void Restart()
        {
            Stop();
            Play();
        }

        // public bool IsPlaying()
        // {
        //     return m_animTweenObjs.Any(tw =>
        //         DOTween.IsTweening(tw.Rect || tw.Rect.GetComponent<Graphic>() || tw.Rect.GetComponent<CanvasGroup>()));
        // }
        
        // public Tween GetLongest()
        // {
        //     Tween longest = null;
        //     foreach (var tweenAnimType in m_tweensMap)
        //     {
        //         if (longest == null)
        //         {
        //             longest = tweenAnimType.Key;
        //             continue;
        //         }
        //
        //         if (tweenAnimType.Key.Duration() > longest.Duration())
        //         {
        //             longest = tweenAnimType.Key;
        //         }
        //     }
        //     
        //     return longest;
        // }

        public bool HaveAnyLoopTween()
        {
            return m_animTweenObjs.Any(e => e.NeedLoop);
        }
    }
}