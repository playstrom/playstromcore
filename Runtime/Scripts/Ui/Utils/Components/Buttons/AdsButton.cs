using System;
using System.Threading;
using System.Threading.Tasks;
using Playstrom.Core.Ads;
using UnityEngine;

namespace Playstrom.Core.Ui
{
    public class AdsButton : ButtonControllerBase
    {
        public event Action OnAdsLoadingError;
        public event Action OnInternetError;
        public event Action OnAdLoaded;

        public void Init(AdsReward adsReward)
        {
            m_isDisabled = false;
            m_adsReward = adsReward;
            m_cancellationTokenSource = new CancellationTokenSource();

            onClick.RemoveAllListeners();
            onClick.AddListener(OnButtonClick);

            m_adsReward.OnRewardedVideoCompleted += ProcessCompleteWatchingAdVideo;

            SetAdsState(m_baseAdsState);

            CheckAdsAvailable();
        }

        public override void Init(Action onClick)
        {
            base.Init(onClick);
            SetAdsState(AdsState.Ready);
        }

        public virtual void DisableAdsGraphic()
        {
            m_cancellationTokenSource?.Cancel();
            SetAdsState(AdsState.Clear);
            m_isDisabled = true;
        }

        public virtual void RemoveAdsLogic()
        {
            onClick.RemoveAllListeners();
            m_adsReward.OnRewardedVideoCompleted -= ProcessCompleteWatchingAdVideo;
        }

        protected override void OnDestroy()
        {
            m_cancellationTokenSource?.Cancel();
            if (m_adsReward != null)
            {
                m_adsReward.OnRewardedVideoCompleted -= ProcessCompleteWatchingAdVideo;
            }

            base.OnDestroy();
        }

        protected virtual void OnButtonClick()
        {
            if (m_adsReward.TryShowAd())
            {
                SetAdsState(m_afterClickAdsState);
            }
            else
            {
                ProcessAdsNotAllowedAfterClick();
            }
        }

        protected virtual void ProcessAdsNotAllowedAfterClick()
        {
            if (Network.IsNetworkAvailable)
            {
                OnAdsLoadingError?.Invoke();
            }
            else
            {
                OnInternetError?.Invoke();
            }

            switch (m_afterClickNoAdsLogic)
            {
                case AfterClickNoAdsLogic.PopUp:
                    break;
                case AfterClickNoAdsLogic.LoadingState:
                    SetAdsState(AdsState.Loading);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected virtual void SetAdsState(AdsState state)
        {
            if (m_isDisabled) return;
            switch (state)
            {
                case AdsState.Loading:
                    if (m_adsDownload != null) m_adsDownload.SetActive(true);
                    if (m_adsIcon != null) m_adsIcon.SetActive(false);
                    break;
                case AdsState.Ready:
                    if (m_adsDownload != null) m_adsDownload.SetActive(false);
                    if (m_adsIcon != null) m_adsIcon.SetActive(true);
                    break;
                case AdsState.Clear:
                    if (m_adsDownload != null) m_adsDownload.SetActive(false);
                    if (m_adsIcon != null) m_adsIcon.SetActive(false);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }

            m_currentState = state;
        }

        private async void CheckAdsAvailable()
        {
            var token = m_cancellationTokenSource.Token;
            Network.CheckConnection();
            while (true)
            {
                await Task.Delay(CheckAdsAvailableTimeMs);
                if (token.IsCancellationRequested)
                {
                    return;
                }

                var adsAllow = Network.IsNetworkAvailable && m_adsReward.IsAllow();

                if (adsAllow)
                {
                    SetAdsState(m_onAdsLoadedState);
                    OnAdLoaded?.Invoke();
                    if (!m_checkNetworkAfterAdLoaded)
                    {
                        break;
                    }
                }
            }
        }

        protected virtual void ProcessCompleteWatchingAdVideo(bool completeWatching)
        {
            SetAdsState(completeWatching ? m_completeWatchState : m_stopWatchState);

            if (!completeWatching)
            {
                m_cancellationTokenSource?.Cancel();
                CheckAdsAvailable();
            }
        }

        [SerializeField, Header("Graphic")] protected GameObject m_adsIcon;
        [SerializeField] protected GameObject m_adsDownload;

        [SerializeField, Header("Ads States")] private AdsState m_baseAdsState;
        [SerializeField] private AdsState m_onAdsLoadedState;
        [SerializeField] private AdsState m_afterClickAdsState;
        [SerializeField] private AdsState m_completeWatchState;
        [SerializeField] private AdsState m_stopWatchState;

        [SerializeField, Header("No Ads States")] private AfterClickNoAdsLogic m_afterClickNoAdsLogic;
        [SerializeField, Header("Logic")] private bool m_checkNetworkAfterAdLoaded;

        protected AdsState m_currentState;

        private CancellationTokenSource m_cancellationTokenSource;
        private AdsReward m_adsReward;
        private bool m_isDisabled;
        private const int CheckAdsAvailableTimeMs = 500;

        [Serializable]
        public enum AdsState
        {
            Loading,
            Ready,
            Clear,
        }

        [Serializable]
        private enum AfterClickNoAdsLogic
        {
            PopUp,
            LoadingState,
        }
    }
}