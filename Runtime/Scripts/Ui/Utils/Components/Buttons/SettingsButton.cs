using UnityEngine;

namespace Playstrom.Core.Ui
{
    public class SettingsButton : ButtonControllerBase
    {
        public void Init(int settingsType, bool defaultState = true)
        {
            m_settingsType = settingsType;
            SetState(defaultState);

            Init(OnClick);

            void OnClick()
            {
                SetState(!SettingState);
            }
        }

        protected virtual void SetGraphicState(bool isOn)
        {
            if (m_onStateSprite == null || m_offStateSprite == null)
            {
                Debug.LogWarning($"No available one of state sprites OnStateSprite {m_onStateSprite}, OffStateSprite {m_offStateSprite}");
                return;
            }

            image.sprite = isOn ? m_onStateSprite : m_offStateSprite;
        }
        
        private void SetState(bool state)
        {
            SettingState = state;
        }

        [SerializeField] private Sprite m_onStateSprite;
        [SerializeField] private Sprite m_offStateSprite;

        private string Key => $"Setting_{m_settingsType}";
        private int m_settingsType = -1;

        private bool SettingState
        {
            get
            {
                return Data.Get<bool>(Key);
            }
            set
            {
                Data.Save(value, Key);
                SetGraphicState(value);
                
                CoreUiEvents.ChangeSettingsState(m_settingsType, value);
            }
        }
    }
}