using System;
using UnityEngine.UI;

namespace Playstrom.Core.Ui
{
    public abstract class ButtonControllerBase : Button
    {
        public virtual void Init(Action onClick)
        {
            this.onClick.RemoveAllListeners();
            this.onClick.AddListener(() => { onClick?.Invoke(); });
        }

        public virtual void SetVisible(bool value, bool isNeedAnimate = false)
        {
            if (value)
            {
                if (isNeedAnimate)
                {
                    //TODO AnimationLogic
                    gameObject.SetActive(true);
                }
                else
                {
                    gameObject.SetActive(true);
                }
            }
            else
            {
                if (isNeedAnimate)
                {
                    //TODO AnimationLogic
                    gameObject.SetActive(false);
                }
                else
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}