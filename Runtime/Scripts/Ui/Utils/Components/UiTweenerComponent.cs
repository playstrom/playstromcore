﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Playstrom.Core.Ui
{
    public class UiTweenerComponent : UiBaseAnimUtil
    {
        [SerializeField] private List<UiTweener> m_tweeners = new List<UiTweener>();

        private UiTweener m_lastPlayedTween = null;

        private bool HaveAnyLoopUiTweener()
        {
            return m_tweeners.Any(tw => tw.HaveAnyLoopTween());
        }

        private float GetLongestUiTweenerTime()
        {
            return m_tweeners.Max(tw => tw.GetLongestTime());
        }

        private async void WaitEndOfPlaying(UiTweener tweener = null, Action action = null)
        {
            if (tweener == null) return;
            if (tweener.HaveAnyLoopTween())
            {
                await Task.Delay(TimeSpan.FromSeconds(tweener.GetLongestTime()));
            }
            else
            {
                while (tweener.IsPlaying)
                {
                    await Task.Delay(100);
                }
            }

            action?.Invoke();
        }

        private async void WaitEndOfAllPlaying(Action action)
        {
            if (HaveAnyLoopUiTweener())
            {
                await Task.Delay(TimeSpan.FromSeconds(GetLongestUiTweenerTime()));
            }
            else
            {
                while (m_tweeners.Any(tw => tw.IsPlaying))
                {
                    await Task.Delay(100);
                }
            }
            action?.Invoke();
        }

        public override void Play(Action<int> playState)
        {
            base.Play(playState);
            foreach (var tweener in m_tweeners)
            {
                tweener.Play();
            }

            WaitEndOfAllPlaying(() => OnPlayEnded(playState));
        }

        public override void PlayByIndex(int index, Action<int> playState)
        {
            base.PlayByIndex(index, playState);
            if (!m_tweeners[index].IsPlaying && m_tweeners[index] != m_lastPlayedTween)
            {
                m_tweeners[index].Play();
                m_lastPlayedTween = m_tweeners[index];
            }

            for (var i = 0; i < m_tweeners.Count; i++)
            {
                if (i == index) continue;
                if (m_tweeners[i].IsPlaying) m_tweeners[i].Stop();
            }
            
            WaitEndOfPlaying(m_tweeners[index], () => OnPlayEnded(playState));
        }

        public override void PlayByType(AnimType type, Action<int> playState)
        {
            base.PlayByType(type, playState);
            List<UiTweener> tweeners = m_tweeners.Where(e => e.AnimType == type).ToList();
            foreach (var tweener in tweeners)
            {
                tweener.Play();
            }
            
            WaitEndOfAllPlaying(() => OnPlayEnded(playState));
        }

        public override void PlayRandom(Action<int> playState)
        {
            base.PlayRandom(playState);
            int index = Random.Range(0, m_tweeners.Count - 1);
            m_tweeners[index].Play();
            WaitEndOfPlaying(m_tweeners[index], () => OnPlayEnded(playState));
        }

        public override void PlayRandomRange(int index1, int index2, Action<int> playState)
        {
            base.PlayRandomRange(index1, index2, playState);
            int index = Random.Range(index1, index2);
            m_tweeners[index].Play();
            WaitEndOfPlaying(m_tweeners[index], () => OnPlayEnded(playState));
        }

        public override void PlayQueue(Action<int> playState)
        {
            Stop();
            base.PlayQueue(playState);
            Sequence seq = DOTween.Sequence();
            seq.SetTarget(this);
            foreach (UiTweener tweener in m_tweeners)
            {
                seq.AppendCallback(tweener.Play);
                seq.AppendInterval(tweener.GetLongestTime());
            }

            seq.OnComplete(() => OnPlayEnded(playState));
        }

        public override void Stop()
        {
            foreach (var tweener in m_tweeners)
            {
                tweener.Stop();
            }
        }

        public override void StopByIndex(int index)
        {
            m_tweeners[index].Stop();
        }

        public override void Restart()
        {
            foreach (var tweener in m_tweeners)
            {
                tweener.Restart();
            }
        }
    }
}