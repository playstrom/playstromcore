﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Playstrom.Core.Ui
{
    /// <summary>
    /// TODO add logic for methods without state callback with base class
    /// </summary>
    [RequireComponent(typeof(Animation))]
    public class UiAnimationComponent : UiBaseAnimUtil
    {
        public bool IsPlaying => m_coroutine != null;

        public override void Stop()
        {
            Animation.Stop();
        }

        public override void StopByIndex(int index)
        {
            Animation.Stop();
        }

        public override void Restart()
        {
            Animation.enabled = true;
            AnimationState state = Animation[Animation.clip.name];
            state.weight = 1;
            state.time = 0;
            Animation.Sample();
            Animation.enabled = false;
        }
        
        public override void Play(Action<int> playState)
        {
            try
            {
                base.Play(playState);
                AnimationClip clip = Animation.clip;
                PlayAnimationClip(clip, () => OnPlayEnded(playState));
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public void Play(Action callback)
        {
            try
            {
                AnimationClip clip = Animation.clip;
                PlayAnimationClip(clip, callback);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public override void PlayByIndex(int index, Action<int> playState)
        {
            try
            {
                base.PlayByIndex(index, playState);
                AnimationClip clip = GetClipByIndex(index);
                PlayAnimationClip(clip, () => OnPlayEnded(playState));
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public void PlayByIndex(int index, Action callback)
        {
            try
            {
                AnimationClip clip = GetClipByIndex(index);
                PlayAnimationClip(clip, callback);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public override void PlayByType(AnimType type, Action<int> playState)
        {
            try
            {
                base.PlayByType(type, playState);
                AnimClipType animType = m_animClipTypes.FirstOrDefault(e => e.AnimType == type);
                if (animType == null)
                {
#if UNITY_EDITOR
                    Debug.LogWarning($"No exist animation on {gameObject.name} with animation type {type}");
#endif
                    return;
                }

                int index = animType.Index;
                AnimationClip clip = GetClipByIndex(index);
                PlayAnimationClip(clip, () => OnPlayEnded(playState));
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public void PlayByType(AnimType type, Action callback)
        {
            try
            {
                AnimClipType animType = m_animClipTypes.FirstOrDefault(e => e.AnimType == type);
                if (animType == null)
                {
#if UNITY_EDITOR
                    Debug.LogWarning($"No exist animation on {gameObject.name} with animation type {type}");
#endif
                    return;
                }

                var index = animType.Index;
                AnimationClip clip = GetClipByIndex(index);
                PlayAnimationClip(clip, callback);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public override void PlayRandom(Action<int> playState)
        {
            try
            {
                base.PlayRandom(playState);
                int index = Random.Range(0, m_states.Count - 1);
                AnimationClip clip = GetClipByIndex(index);
                PlayAnimationClip(clip, () => OnPlayEnded(playState));
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public void PlayRandom(Action callback)
        {
            try
            {
                int index = Random.Range(0, m_states.Count - 1);
                AnimationClip clip = GetClipByIndex(index);
                PlayAnimationClip(clip, callback);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public override void PlayRandomRange(int index1, int index2, Action<int> playState)
        {
            try
            {
                base.PlayRandomRange(index1, index2, playState);
                int index = Random.Range(index1, index2);
                AnimationClip clip = GetClipByIndex(index);
                PlayAnimationClip(clip, () => OnPlayEnded(playState));
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public void PlayRandomRange(int index1, int index2, Action callback)
        {
            try
            {
                int index = Random.Range(index1, index2);
                AnimationClip clip = GetClipByIndex(index);
                PlayAnimationClip(clip, callback);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public override void PlayQueue(Action<int> playState)
        {
            try
            {
                base.PlayQueue(playState);
                foreach (var state in m_states)
                {
                    Animation.PlayQueued(state.name, QueueMode.CompleteOthers);
                }

                PlayAnimationClip(null, () => OnPlayEnded(playState));
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        public void PlayQueue(Action callback)
        {
            try
            {
                foreach (var state in m_states)
                {
                    Animation.PlayQueued(state.name, QueueMode.CompleteOthers);
                }

                PlayAnimationClip(null, callback);
            }
            catch (Exception e)
            {
                Debug.LogWarning(e.Message);
                return;
            }
        }

        private void Awake()
        {
            InitAnimationStates();
        }

        private void InitAnimationStates()
        {
            foreach (AnimationState animState in Animation)
            {
                m_states.Add(animState);
            }
        }

        private void PlayAnimationClip(AnimationClip clip, Action action = null)
        {
            if (!gameObject.activeInHierarchy)
            {
                Debug.LogWarning("Try to play animation on disabled object");
                return;
            }

            if (clip == null)
            {
                Debug.LogWarning("Trying to play Null clip");
                return;
            }

            if (m_coroutine != null)
            {
                StopCoroutine(m_coroutine);
                m_coroutine = null;
                m_lastAction?.Invoke();
            }

            Animation.Stop();
            Animation.clip = clip;
            Animation.Play(clip.name);

            m_lastAction = action;
            m_coroutine = StartCoroutine(WaitEndOfPlaying(clip, action));
        }

        private AnimationClip GetClipByIndex(int index)
        {
            bool isCorrectIndex = index >= 0 && index < m_states.Count;
            return isCorrectIndex ? m_states[index].clip : null;
        }

        private IEnumerator WaitEndOfPlaying(AnimationClip clip, Action action)
        {
            if (clip.wrapMode == WrapMode.Loop)
            {
#if UNITY_EDITOR
                Debug.LogWarning($"{clip.name} is looping");
#endif
            }

            while (Animation.IsPlaying(clip.name))
            {
                yield return new WaitForEndOfFrame();
            }

            m_coroutine = null;
            m_lastAction = null;
            action?.Invoke();
        }

        [SerializeField] private List<AnimClipType> m_animClipTypes = new List<AnimClipType>();

        [Serializable]
        private class AnimClipType
        {
            [SerializeField] private int m_index = 0;
            [SerializeField] private AnimType m_animType = AnimType.None;

            public int Index => m_index;
            public AnimType AnimType => m_animType;
        }

        private Animation m_animation;
        private readonly List<AnimationState> m_states = new List<AnimationState>();
        private Coroutine m_coroutine;
        private Action m_lastAction;

        private Animation Animation
        {
            get
            {
                if (m_animation == null)
                {
                    m_animation = GetComponent<Animation>();
                }

                return m_animation;
            }
        }
    }
}