﻿using System;
using UnityEngine.Events;

namespace Playstrom.Core.Ui
{
    [Serializable]
    public class UnityEventAnimType : UnityEvent<AnimType, Action<int>>
    {
    }

    public class UiAnimEventHandler
    {
        private int m_counter = 0;
        private Action m_callback = null;

        private void Counter(int count)
        {
            m_counter += count;
            if (m_counter == 0)
            {
                m_callback?.Invoke();
            }
        }
        
        private UiAnimEventHandler(UnityEventAnimType eventAnim, AnimType animType, Action callback = null)
        {
            m_callback = callback;
            m_counter = 0;
            eventAnim?.Invoke(animType, Counter);
            Counter(0);
        }
        
        public static UiAnimEventHandler Invoke(UnityEventAnimType changeButtonState, AnimType type, Action callback = null)
        {
            return new UiAnimEventHandler(changeButtonState, type, callback);
        }
    }
}
