using System.IO;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

namespace Playstrom.Core.Ui
{
    public static class Data
    {
        public static void Save<T>(T value, string key)
        {
            var json = JsonConvert.SerializeObject(value);
            PlayerPrefs.SetString(key, json);
        }

        public static void SaveToFile<T>(T value, string key)
        {
            var json = JsonConvert.SerializeObject(value);
            var fileName = $"{key}.json";

            if (!Directory.Exists(FileDataPath))
            {
                Directory.CreateDirectory(FileDataPath);
            }
#if UNITY_EDITOR
            AssetDatabase.Refresh();
#endif

            File.WriteAllText(FileDataPath + fileName, json);
#if UNITY_EDITOR
            AssetDatabase.Refresh();
#endif
        }

        public static T LoadFromFile<T>(string key)
        {
            var fileName = $"{key}.json";
            if (File.Exists(FileDataPath + fileName))
            {
                var json = File.ReadAllText(FileDataPath + fileName);
                return JsonConvert.DeserializeObject<T>(json);
            }

            Logger.Warning($"File not found with name : {key} on path : {FileDataPath}");
            return default;
        }

        public static T Get<T>(string key)
        {
            if (!PlayerPrefs.HasKey(key))
            {
                Logger.Warning($"No exist data with key {key}");
                return default;
            }

            var data = PlayerPrefs.GetString(key);
            return JsonConvert.DeserializeObject<T>(data);
        }

        public static bool Exist(string key)
        {
            return PlayerPrefs.HasKey(key);
        }

#if !UNITY_EDITOR
        private static string FileDataPath = Application.persistentDataPath + "/FileData/";
#else
        private static string FileDataPath = Application.dataPath + "/FileData/";
#endif
    }
}