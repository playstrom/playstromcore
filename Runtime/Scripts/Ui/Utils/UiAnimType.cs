﻿using System;

namespace Playstrom.Core.Ui
{
    [Serializable]
    public enum AnimType
    {
        None = 0,
        Normal = 1,
        Pressed = 2,
        Disable = 3,
        Show = 4,
        Hide = 5,
        AdsAvailable = 6,
        AdsNotAvailable = 7
    }
}