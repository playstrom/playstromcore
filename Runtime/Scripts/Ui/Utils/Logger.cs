using System;
using UnityEngine;

namespace Playstrom.Core.Ui
{
    [Serializable]
    public enum LogLevel
    {
        Log = 0,
        Disabled = 1,
        FullLog = 2,
    }

    public static class Logger
    {
        public static void SetLogLevel(LogLevel level)
        {
            m_logLevel = level;
        }

        public static void Message(string message, string title = null)
        {
            if (CanShowMessage())
            {
                Debug.Log($"PlaystromLog Message {title} : {message}");
            }
        }

        public static void Warning(string message, string title = null)
        {
            if (CanShowMessage())
            {
                Debug.LogWarning($"PlaystromLog Warning {title} : {message}");
            }
        }

        public static void Error(string message, string title = null)
        {
            if (CanShowMessage())
            {
                Debug.LogError($"PlaystromLog Error {title} : {message}");
            }
        }

        private static bool CanShowMessage()
        {
#if UNITY_EDITOR
            return true;
#endif
            if (Debug.isDebugBuild)
            {
                return true;
            }

            return m_logLevel switch
            {
                LogLevel.FullLog => true,
                LogLevel.Disabled => false,
                _ => true
            };
        }

        private static LogLevel m_logLevel = LogLevel.Log;
    }
}