using UnityEditor;
using UnityEditor.UI;

namespace Playstrom.Core.Ui
{
    [CustomEditor(typeof(AdsButton))]
    public class AdsButtonEditor : ButtonEditor
    {
        private SerializedProperty m_adsIcon;
        private SerializedProperty m_adsDownload;
        
        private SerializedProperty m_baseAdsState;
        private SerializedProperty m_onAdsLoadedState;
        private SerializedProperty m_afterClickAdsState;
        private SerializedProperty m_completeWatchState;
        private SerializedProperty m_stopWatchState;

        
        private SerializedProperty m_afterClickNoAdsLogic;

        private SerializedProperty m_checkNetworkAfterAdLoaded;
        
        protected override void OnEnable()
        {
            base.OnEnable();
            
            m_adsIcon = serializedObject.FindProperty("m_adsIcon");
            m_adsDownload = serializedObject.FindProperty("m_adsDownload");

            m_baseAdsState = serializedObject.FindProperty("m_baseAdsState");
            m_onAdsLoadedState = serializedObject.FindProperty("m_onAdsLoadedState");
            m_afterClickAdsState = serializedObject.FindProperty("m_afterClickAdsState");
            m_completeWatchState = serializedObject.FindProperty("m_completeWatchState");
            m_stopWatchState = serializedObject.FindProperty("m_stopWatchState");
            
            m_afterClickNoAdsLogic = serializedObject.FindProperty("m_afterClickNoAdsLogic");
            
            m_checkNetworkAfterAdLoaded = serializedObject.FindProperty("m_checkNetworkAfterAdLoaded");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            
            EditorGUILayout.PropertyField(m_adsIcon);
            EditorGUILayout.PropertyField(m_adsDownload);
            
            EditorGUILayout.Space();
            
            EditorGUILayout.PropertyField(m_baseAdsState);
            EditorGUILayout.PropertyField(m_onAdsLoadedState);
            EditorGUILayout.PropertyField(m_afterClickAdsState);
            EditorGUILayout.PropertyField(m_completeWatchState);
            EditorGUILayout.PropertyField(m_stopWatchState);

            EditorGUILayout.Space();
            
            EditorGUILayout.PropertyField(m_afterClickNoAdsLogic);
            
            EditorGUILayout.Space();
            
            EditorGUILayout.PropertyField(m_checkNetworkAfterAdLoaded);

            serializedObject.ApplyModifiedProperties();
        }
    }
}