using UnityEditor;
using UnityEditor.UI;

namespace Playstrom.Core.Ui
{
    [CustomEditor(typeof(SettingsButton))]
    public class SettingsButtonEditor : ButtonEditor
    {
        private SerializedProperty m_onStateSprite;
        private SerializedProperty m_offStateSprite;

        protected override void OnEnable()
        {
            base.OnEnable();
            
            m_onStateSprite = serializedObject.FindProperty("m_onStateSprite");
            m_offStateSprite = serializedObject.FindProperty("m_offStateSprite");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();
            
            EditorGUILayout.PropertyField(m_onStateSprite);
            EditorGUILayout.PropertyField(m_offStateSprite);

            serializedObject.ApplyModifiedProperties();
        }
    }
}