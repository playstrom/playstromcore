﻿using UnityEditor;
using UnityEditor.UI;

namespace Playstrom.Core.Ui
{
    [CustomEditor(typeof(ButtonController))]
    public class ButtonControllerEditor : ButtonEditor
    {
        SerializedProperty m_changeButtonState = null;
        SerializedProperty m_showHide = null;

        SerializedProperty m_buttonType = null;
        SerializedProperty m_isSetting = null;
        SerializedProperty m_onStateSprite = null;
        SerializedProperty m_offStateSprite = null;
        
        SerializedProperty m_isAds = null;
        SerializedProperty m_adsIcon = null;
        SerializedProperty m_adsDownload = null;
        SerializedProperty m_changeAdsState = null;

        protected override void OnEnable()
        {
            base.OnEnable();
            m_changeButtonState = serializedObject.FindProperty("m_changeButtonState");
            m_showHide = serializedObject.FindProperty("m_showHide");

            m_buttonType = serializedObject.FindProperty("m_buttonType");
            m_isSetting = serializedObject.FindProperty("m_isSetting");
            m_onStateSprite = serializedObject.FindProperty("m_onStateSprite");
            m_offStateSprite = serializedObject.FindProperty("m_offStateSprite");
            
            m_isAds = serializedObject.FindProperty("m_isAds");
            m_adsIcon = serializedObject.FindProperty("m_adsIcon");
            m_adsDownload = serializedObject.FindProperty("m_adsDownload");
            m_changeAdsState = serializedObject.FindProperty("m_changeAdsState");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.Space();
            serializedObject.Update();
            EditorGUILayout.PropertyField(m_changeButtonState);

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(m_showHide);

            EditorGUILayout.PropertyField(m_buttonType);
            switch (m_buttonType.enumValueIndex)
            {
                case 1:
                    EditorGUILayout.PropertyField(m_onStateSprite);
                    EditorGUILayout.PropertyField(m_offStateSprite);
                    break;
                case 2:
                    EditorGUILayout.PropertyField(m_adsIcon);
                    EditorGUILayout.PropertyField(m_adsDownload);
                    EditorGUILayout.PropertyField(m_changeAdsState);
                    break;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
