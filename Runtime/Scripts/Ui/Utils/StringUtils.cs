﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;


namespace Playstrom.Core.Ui
{
    public static class StringUtils
    {
        #region NumbersStyle

        //comma separation
        public static string ToStringWithComma<T>(this T value)
        {
            var stringValue = value.ToString();
            for (int i = 12; i > 0; i -= 3)
            {
                if (stringValue.Length > i)
                {
                    stringValue = stringValue.Insert(stringValue.Length - i, ",");
                }
            }

            return stringValue;
        }

        //dot separation
        public static string ToStringWithDot<T>(this T value)
        {
            var stringValue = value.ToString();
            for (int i = 12; i > 0; i -= 3)
            {
                if (stringValue.Length > i)
                {
                    stringValue = stringValue.Insert(stringValue.Length - i, ".");
                }
            }

            return stringValue;
        }

        #endregion

        #region Coordinates

        public static string GetCoordinatesXY(Vector2 coord)
        {
            return $"x:{coord.x} y:{coord.y}";
        }

        public static string GetCoordinatesXYZ(Vector3 coord)
        {
            return $"x:{coord.x} y:{coord.y} z:{coord.z}";
        }

        #endregion

        #region Numbers

        public enum NumberSystem
        {
            Binary,
            Octal,
            Hexadecimal
        }

        //roman numbers
        public static string IntToRoman(int num)
        {
            string result = string.Empty;
            var map = new Dictionary<string, int>
            {
                {"M", 1000},
                {"CM", 900},
                {"D", 500},
                {"CD", 400},
                {"C", 100},
                {"XC", 90},
                {"L", 50},
                {"XL", 40},
                {"X", 10},
                {"IX", 9},
                {"V", 5},
                {"IV", 4},
                {"I", 1}
            };
            foreach (var pair in map)
            {
                result += string.Join(string.Empty, Enumerable.Repeat(pair.Key, num / pair.Value));
                num %= pair.Value;
            }

            return result;
        }

        public static string IntToOtherSystem(int intValue, NumberSystem system)
        {
            return Convert.ToString(intValue, GetNumberSystemValue(system));
        }

        public static int OtherSystemToInt(string value, NumberSystem system)
        {
            return Convert.ToInt32(value, GetNumberSystemValue(system));
        }

        private static int GetNumberSystemValue(NumberSystem system)
        {
            switch (system)
            {
                case NumberSystem.Binary:
                    return 2;
                case NumberSystem.Octal:
                    return 8;
                case NumberSystem.Hexadecimal:
                    return 16;
            }

            return 10;
        }

        #endregion

        #region Color

        public static string RGBToHex(Color color)
        {
            return $"{ToByte(color.r):X2}{ToByte(color.g):X2}{ToByte(color.b):X2}";
        }

        public static Color HexToRGB(string color)
        {
            //Remove # if present
            if (color.IndexOf('#') != -1)
                color = color.Replace("#", "");

            int red = 0;
            int green = 0;
            int blue = 0;

            switch (color.Length)
            {
                case 6:
                    //#RRGGBB
                    red = int.Parse(color.Substring(0, 2), NumberStyles.AllowHexSpecifier);
                    green = int.Parse(color.Substring(2, 2), NumberStyles.AllowHexSpecifier);
                    blue = int.Parse(color.Substring(4, 2), NumberStyles.AllowHexSpecifier);
                    break;
                case 3:
                    //#RGB
                    red = int.Parse(color[0].ToString() + color[0].ToString(), NumberStyles.AllowHexSpecifier);
                    green = int.Parse(color[1].ToString() + color[1].ToString(), NumberStyles.AllowHexSpecifier);
                    blue = int.Parse(color[2].ToString() + color[2].ToString(), NumberStyles.AllowHexSpecifier);
                    break;
            }

            return new Color(red, green, blue);
        }

        private static byte ToByte(float f)
        {
            f = Mathf.Clamp01(f);
            return (byte) (f * 255);
        }

        #endregion

        #region Abbreviations

        //size of bytes
        public static string GetReadableSize(long byteSize)
        {
            string[] sizes = StringUtilsTranslate.GetSizeTranslate();
            double len = byteSize;
            int order = 0;
            while (len >= 1024 && order < sizes.Length - 1)
            {
                order++;
                len = len / 1024;
            }

            string result = $"{len:0.##} {sizes[order]}";
            return result;
        }

        //to K,M,B,T...
        public static string GetShortedFromDouble(double val, bool useRValue = true)
        {
            string separator = ".";
            string[] shortCuts =
                {"", "K", "M", "B", "T", "q", "Q", "s", "S", "O", "N", "d", "U", "D", "!", "@", "#"};
            double mainNum = 1;

            int idx = 0;
            while (val + 0.01 >= mainNum * 1000.0)
            {
                idx++;
                mainNum *= 1000.0;
            }

            string postfix = "";

            if (idx >= shortCuts.Length)
            {
                postfix += shortCuts[shortCuts.Length - 1];
                if (idx - shortCuts.Length >= shortCuts.Length)
                {
                    return "MaxNum";
                }

                postfix += shortCuts[idx - shortCuts.Length];
            }
            else
            {
                postfix = shortCuts[idx];
            }

            double result = val / mainNum;
            int leftVal = (int) result;
            string leftValStr = leftVal.ToString();
            int leftValSize = leftValStr.Length;
            int rightVal = idx == 0 ? 0 : (int) ((result - leftVal) * Math.Pow(10.0, 3 - leftValSize));

            if (useRValue)
            {
                if (3 - leftValSize > 0)
                {
                    string rv = "";

                    if (rightVal == 0)
                    {
                        if (idx > 0)
                        {
                            for (int i = 0; i < 3 - leftValSize; ++i)
                            {
                                rv += "0";
                            }
                        }
                    }
                    else
                    {
                        int zerosCount = 3 - leftValSize - rightVal.ToString().Length;

                        string zerosStr = "";
                        for (int i = 0; i < zerosCount; ++i)
                        {
                            zerosStr += "0";
                        }

                        rv = zerosStr + rightVal;
                    }

                    if (idx > 0)
                    {
                        return leftVal + separator + rv + postfix;
                    }

                    return leftVal + postfix;
                }
            }

            return leftVal + postfix;
        }

        #endregion

        #region Time

        // time D H:MM:SS or D:HH:MM:SS
        public static string GetTimeFormated(TimeSpan span, bool cutDays = false, bool isFull = false)
        {
            string[] shortAbbr = StringUtilsTranslate.GetTimeTranslate(isFull);
            if (cutDays && span.Duration().Days > 0)
            {
                return
                    $"{(span.Duration().Days > 0 ? $"{span.Days}{shortAbbr[0]} " : string.Empty)}" +
                    $"{(span.Duration().Hours > 0 ? $"{span.Hours:D2}:" : string.Empty)}" +
                    $"{span.Minutes:D2}:" +
                    $"{span.Seconds:D2}";
            }

            return
                $"{(span.Duration().Days > 0 ? $"{span.Days}:" : string.Empty)}" +
                $"{(span.Duration().Hours > 0 ? $"{span.Hours:D2}:" : string.Empty)}" +
                $"{span.Minutes:D2}:" +
                $"{span.Seconds:D2}";
        }

        // time 1m 1s
        public static string GetTime(TimeSpan span, bool isFull = false)
        {
            string[] shortAbbr = StringUtilsTranslate.GetTimeTranslate(isFull);
            return
                $"{(span.Duration().Days > 0 ? $"{span.Days}{shortAbbr[0].ToLower()} " : string.Empty)}" +
                $"{(span.Duration().Hours > 0 ? $"{span.Hours}{shortAbbr[1].ToLower()} " : string.Empty)}" +
                $"{(span.Duration().Minutes > 0 ? $"{span.Minutes}{shortAbbr[2].ToLower()} " : string.Empty)}" +
                $"{span.Seconds}{shortAbbr[3].ToLower()}";
        }

        #endregion
    }

    public static class StringUtilsTranslate
    {
        public static string[] GetSizeTranslate()
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Russian:
                case SystemLanguage.Belarusian:
                case SystemLanguage.Ukrainian:
                    return new[] {"Б", "КБ", "МБ", "ГБ", "ТВ"};
                case SystemLanguage.French:
                    return new[] {"B", "Ko", "Mo", "Go", "To"};
                default:
                    return new[] {"B", "KB", "MB", "GB", "TB"};
            }
        }

        public static string[] GetTimeTranslate(bool isFull = false)
        {
            switch (Application.systemLanguage)
            {
                case SystemLanguage.English:
                    return isFull ? new[] {" days", " hours", " minutes", " seconds"} : new[] {"D", "H", "M", "S"};
                case SystemLanguage.French:
                    return isFull ? new[] {" jours", " heures", " minutes", " secondes"} : new[] {"J", "H", "M", "S"};
                case SystemLanguage.German:
                    return isFull ? new[] {" Tage", " Stunden", " Minuten", " Secunden"} : new[] {"T", "S", "M", "S"};
                case SystemLanguage.Italian:
                    return isFull ? new[] {" giorni", " hore", " minuti", " secondi"} : new[] {"GG", "H", "M", "S"};
                case SystemLanguage.Japanese:
                    return new[] {"日", "時間", "分", "秒"};
                case SystemLanguage.Korean:
                    return new[] {"일", "시간", "분", "초"};
                case SystemLanguage.Portuguese:
                    return isFull ? new[] {" dias", " horas", " minutos", " segundos"} : new[] {"D", "H", "M", "S"};
                case SystemLanguage.Russian:
                    return isFull ? new[] {" дней", " часов", " минут", " секунд"} : new[] {"Д", "Ч", "М", "С"};
                case SystemLanguage.Spanish:
                    return isFull ? new[] {" días", " horas", " minutos", " segundos"} : new[] {"D", "H", "M", "S"};
                case SystemLanguage.Chinese:
                case SystemLanguage.ChineseSimplified:
                case SystemLanguage.ChineseTraditional:
                    return new[] {"天", "小时", "分钟", "秒"};
                default:
                    return new[] {"D", "H", "M", "S"};
            }
        }
    }
}