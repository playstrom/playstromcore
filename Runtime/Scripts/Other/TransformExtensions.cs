using System.Collections.Generic;
using UnityEngine;

namespace Playstrom.Core.Extensions
{
    public static class TransformExtensions
    {
        public static List<Transform> GetAllChilds(this Transform parent)
        {
            var transforms = new List<Transform>();
            foreach (Transform t in parent)
            {
                transforms.Add(t);
            }

            return transforms;
        }
        
        public static Transform Clear(this Transform transform)
        {
            foreach (Transform child in transform) {
                GameObject.Destroy(child.gameObject);
            }
            return transform;
        }

        public static Vector3 GetPositionOnScreen(Transform worldTransform, GameObject canvasObject)
        {
            var pos = Camera.main.WorldToScreenPoint(worldTransform.position);

            var canvas = (RectTransform)canvasObject.transform;

            var rect = canvas.rect;
            var scale = new Vector2(
                rect.width / Screen.width,
                rect.height / Screen.height
            );

            pos = Vector2.Scale(pos, scale);

            pos.z = 0;
            return pos;
        }
        
        public static RectTransform Left(this RectTransform rt, float x)
        {
            rt.offsetMin = new Vector2(x, rt.offsetMin.y);
            return rt;
        }

        public static RectTransform Right(this RectTransform rt, float x)
        {
            rt.offsetMax = new Vector2(-x, rt.offsetMax.y);
            return rt;
        }

        public static RectTransform Bottom(this RectTransform rt, float y)
        {
            rt.offsetMin = new Vector2(rt.offsetMin.x, y);
            return rt;
        }

        public static RectTransform Top(this RectTransform rt, float y)
        {
            rt.offsetMax = new Vector2(rt.offsetMax.x, -y);
            return rt;
        }
    }
}