using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Playstrom.Core
{
    public static class Network
    {
        public static bool IsNetworkAvailable;

        public static async void CheckConnection()
        {
#if UNITY_EDITOR
            return;
#endif
            if (m_needToCheckConnection == false)
            {
                IsNetworkAvailable = true;
                return;
            }
            if (m_cancellationTokenSource != null) return;
            m_cancellationTokenSource = new CancellationTokenSource();
            var token = m_cancellationTokenSource.Token;
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    return;
                }

                IsNetworkAvailable = await IsSuccessNetwork();
                await Task.Delay(NextTimeNetworkCheckDelayMs);
            }
        }

        /// <summary>
        /// Method to stop ping server
        /// </summary>
        public static void StopCheckConnection()
        {
            m_cancellationTokenSource?.Cancel();
        }

        public static void SetCheckConnectionState(bool state)
        {
            m_needToCheckConnection = state;
        }

        private static async Task<bool> IsSuccessNetwork()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                await Task.Delay(NetworkNotReachableDelayMs);
                return false;
            }

            return await CheckInternetConnection();
        }

        private static async Task<bool> CheckInternetConnection()
        {
            var request = UnityWebRequest.Head(EchoServer);
            request.timeout = 5;
            await request.SendWebRequest();

            var result = request.result != UnityWebRequest.Result.ConnectionError &&
                         request.result != UnityWebRequest.Result.ProtocolError &&
                         request.responseCode == 200;
            return result;
        }

        private static bool m_needToCheckConnection;
        private static CancellationTokenSource m_cancellationTokenSource;
        
        private const int NetworkNotReachableDelayMs = 1000;
        private const int NextTimeNetworkCheckDelayMs = 5000;
        private const string EchoServer = "https://google.com";
    }
}