using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Playstrom.Core.Extensions
{
    public class Screenshot : MonoBehaviour
    {
        /// <summary>
        /// Taken texture will not be destroyed by GC, so u should control it
        /// </summary>
        /// <param name="camera"></param>
        /// <returns></returns>
        public static Texture2D Take(Camera camera)
        {
            // The Render Texture in RenderTexture.active is the one
            // that will be read by ReadPixels.
            var currentRT = RenderTexture.active;
            RenderTexture.active = camera.targetTexture;

            // Render the camera's view.
            camera.Render();

            // Make a new texture and read the active Render Texture into it.
            var targetTexture = camera.targetTexture;
            targetTexture.name = $"TargetTexture";
            var takenTexture = new Texture2D(targetTexture.width, targetTexture.height);
            takenTexture.ReadPixels(new Rect(0, 0, targetTexture.width, targetTexture.height), 0, 0);
            takenTexture.Apply();

            // Replace the original active Render Texture.
            RenderTexture.active = currentRT;
            return takenTexture;
        }

        public static Texture2D Resize(Texture2D texture2D, int targetX, int targetY, int depth = 24)
        {
            var rt = new RenderTexture(targetX, targetY, depth);
            RenderTexture.active = rt;
            Graphics.Blit(texture2D, rt);
            var result = new Texture2D(targetX, targetY, TextureFormat.RGBA32, false);
            result.ReadPixels(new Rect(0, 0, targetX, targetY), 0, 0);
            result.Apply();
            return result;
        }
    }
}