using UnityEngine;
using UnityEngine.Pool;

namespace Playstrom.Core
{
    public interface IPoolable
    {
        void InitPool(IObjectPool<GameObject> pool);
        void Release();
    }

    public enum PoolType
    {
        Stack,
        LinkedList
    }

    public class CustomPool<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    var coreGameObject = new GameObject(typeof(T).Name);
                    instance = coreGameObject.AddComponent<T>();
                }

                return instance;
            }
        }

        public IObjectPool<GameObject> Pool
        {
            get
            {
                if (_pool == null)
                {
                    InitPool();
                }

                return _pool;
            }
        }
        
        protected virtual void Awake()
        {
            if (instance == null)
            {
                instance = GetComponent<T>();
            }
            else
            {
                DestroyImmediate(this);
            }
        }

        protected virtual void Start()
        {
            InitPool();
            DontDestroyOnLoad(gameObject);

            for (var i = 0; i < _defaultCapacity; i++)
            {
                var obj = CreatePooledItem();
                obj.GetComponent<IPoolable>().Release();
            }
        }

        protected virtual void InitPool()
        {
            if (_poolType == PoolType.Stack)
                _pool = new ObjectPool<GameObject>(CreatePooledItem, OnTakeFromPool, OnReturnedToPool,
                    OnDestroyPoolObject, _collectionChecks, _defaultCapacity, _maxPoolSize);
            else
                _pool = new LinkedPool<GameObject>(CreatePooledItem, OnTakeFromPool, OnReturnedToPool,
                    OnDestroyPoolObject, _collectionChecks, _maxPoolSize);
        }

        protected virtual GameObject CreatePooledItem()
        {
            var item = Instantiate(_mainObject);
            var IPoolable = item.GetComponent<IPoolable>();
            IPoolable.InitPool(_pool);
            return item.gameObject;
        }

        protected virtual void OnReturnedToPool(GameObject poolItem)
        {
            poolItem.transform.SetParent(transform);
            poolItem.gameObject.SetActive(false);
        }

        protected virtual void OnTakeFromPool(GameObject poolItem)
        {
            poolItem.gameObject.SetActive(true);
        }

        protected virtual void OnDestroyPoolObject(GameObject poolItem)
        {
            Destroy(poolItem.gameObject);
        }

        [SerializeField] private GameObject _mainObject;
        [SerializeField] private int _defaultCapacity = 4;
        [SerializeField] private PoolType _poolType;
        [SerializeField] private bool _collectionChecks = true;
        [SerializeField] private int _maxPoolSize = 25;

        private static T instance;

        private IObjectPool<GameObject> _pool;
    }
}