using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Networking;

namespace Playstrom.Core
{
    public class UnityWebRequestAwaiter : INotifyCompletion
    {
        private UnityWebRequestAsyncOperation asyncOperation;
        private Action callback;

        public UnityWebRequestAwaiter(UnityWebRequestAsyncOperation asyncOperation)
        {
            this.asyncOperation = asyncOperation;
            asyncOperation.completed += OnRequestCompleted;
        }

        public bool IsCompleted => asyncOperation.isDone;

        public void GetResult()
        {
        }

        public void OnCompleted(Action callback)
        {
            this.callback = callback;
        }

        private void OnRequestCompleted(AsyncOperation obj)
        {
            callback?.Invoke();
        }
    }

    public static class ExtensionMethods
    {
        public static UnityWebRequestAwaiter GetAwaiter(this UnityWebRequestAsyncOperation asyncOp)
        {
            return new UnityWebRequestAwaiter(asyncOp);
        }
    }
}