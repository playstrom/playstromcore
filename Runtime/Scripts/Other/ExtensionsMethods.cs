﻿using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor.Presets;
#endif
using UnityEngine;

namespace Playstrom.Core
{
    public static class ExtensionsMethods
    {
        public static T RandomFromArray<T>(this T[] array)
        {
            return array[Random.Range(0, array.Length)];
        }

        public static T RandomFromList<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }
#if UNITY_EDITOR
        public static T AddComponentWithPresets<T>(this GameObject targetObject) where T : Component
        {
            var component = targetObject.AddComponent<T>();
            var presets = Preset.GetDefaultPresetsForObject(component);
            if (presets.Length > 0)
            {
                Debug.LogWarning($"Presets more than one on {targetObject.name} with {typeof(T)}");
                presets.First().ApplyTo(component);
            }

            return component;
        }
#endif
        public static void SetParentAndSetDefault(this Transform transform, Transform parent)
        {
            transform.SetParent(parent);
            transform.localScale = Vector3.one;
            transform.localPosition = Vector3.zero;
        }
    }
}