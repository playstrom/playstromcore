using System.Collections.Generic;
using ModestTree;
using Zenject;

namespace Playstrom.Core.Extensions
{
    public class ZenjectExtensions
    {
         public static void AddPanelsParams(string key, object data)
        {
            if (data == null)
            {
                return;
            }
            
            var container = ProjectContext.Instance.Container;
            
            if (container.HasBindingId(data.GetType(), key))
            {
                container.UnbindId(data.GetType(), key);
            }

            container.Bind(data.GetType()).WithId(key).FromInstance(data);
        }
        
        public static void AddPanelsParams(Dictionary<string, object> addParams = null)
        {
            if (addParams == null) return;
            var container = ProjectContext.Instance.Container;
            foreach (var addParam in addParams)
            {
                if (container.HasBindingId(addParam.Value.GetType(), addParam.Key))
                {
                    container.UnbindId(addParam.Value.GetType(), addParam.Key);
                }

                container.Bind(addParam.Value.GetType()).WithId(addParam.Key).FromInstance(addParam.Value);
            }
        }
        
        public static void AddPanelsParamsWithInterfaceBinding(Dictionary<string, object> addParams = null)
        {
            if (addParams == null) return;
            var container = ProjectContext.Instance.Container;
            foreach (var addParam in addParams)
            {
                var paramInterfaces = addParam.Value.GetType().Interfaces();
                foreach (var paramInterface in paramInterfaces)
                {
                    if (container.HasBindingId(paramInterface, addParam.Key))
                    {
                        container.UnbindId(paramInterface, addParam.Key);
                    }

                    container.Bind(paramInterface).WithId(addParam.Key).FromInstance(addParam.Value);
                }
            }
        }
        
        public static T GetDataFromContainer<T>(string key)
        {
            var container = ProjectContext.Instance.Container;
            return container.ResolveId<T>(key);
        }
    }
}