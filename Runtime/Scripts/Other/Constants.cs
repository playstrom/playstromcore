﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
   #region Managers

   public const string MANAGERS = "Managers";
   public const string UI_MANAGER = "UiManager";
   public const string DATA_MANAGER = "DataManager";
   public const string CONFIG_MANAGER = "ConfigManager";
   public const string EVENT_MANAGER = "EventManager";
   public const string GAME_PLAY_MANAGER = "GamePlayManager";
   public const string MANAGERS_CONFIG = "ManagersConfig";
   public const string APP_MANAGER = "AppManager";
   public const string META_MANAGER = "MetaManager";

   #endregion

   #region Base

   public const string SLASH = "/";

   public const int ZERO = 0;
   public const int ONE = 1;

   #endregion

   #region Rewards

   public const string REWARD = "Reward";
   public const string REWARD_ITEM_BASE = "RewardItemBase";
   public const string REWARD_ITEM_CONFIG = "RewardItemConfig";
   public const string REWARD_CONFIG = "RewardConfig";

   #endregion
}
