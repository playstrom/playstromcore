using System;
using UnityEditor;
using UnityEngine;

namespace Playstrom.Core
{
    [Serializable]
    public class ItemResource
    {
        public T GetItem<T>() where T : UnityEngine.Object
        {
            if (m_itemPath == string.Empty)
            {
#if UNITY_EDITOR
                Debug.Log($"{this.GetType()}<color=red> Null path </color>");
#endif
                return null;
            }

            return Resources.Load<T>(m_itemPath);
        }
#if UNITY_EDITOR
        public void Repeat(object obj = null)
        {
            if (obj != null)
            {
                m_itemEditor = obj;
                Debug.LogWarning($"{this.GetType()}<color=green> Object <b>{obj}</b> setup complete</color>");
            }

            SetPath();
        }

        public void SetPath()
        {
            var path = GetPath(m_itemEditor);

            if (path == string.Empty)
            {
                return;
            }

            m_itemPath = path;
        }

        public static string GetPath(object obj)
        {
            if (obj == null)
            {
                Debug.Log($"ItemPathResources <color=red> Object null </color>");
                return string.Empty;
            }

            var path = AssetDatabase.GetAssetPath((UnityEngine.Object)obj);
            var sub = path.Substring(0, path.LastIndexOf("Resources/", StringComparison.Ordinal));
            path = path.Replace(sub + "Resources/", string.Empty);
            path = path.Substring(0, path.LastIndexOf(".", StringComparison.Ordinal));
            Debug.Log($"ItemPathResources <color=green>Find path <b>{path}</b> for object</color>");
            return path;
        }

        [Header("Should be in Resources")] public object m_itemEditor;
#endif
        [SerializeField] public string m_itemPath;
    }
}