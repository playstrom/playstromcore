using System;

namespace Playstrom.Core
{
    public class NotifiedProperty<T> : IProperty<T>
    {
        public static implicit operator T(NotifiedProperty<T> notifiedProperty)
        {
            return notifiedProperty.Value;
        }

        public T Value
        {
            get => m_value;
            set
            {
                m_value = value;
                OnValueChanged?.Invoke(value);
            }
        }

        public event Action<T> OnValueChanged;

        private T m_value;
    }

    public interface IProperty<T> : IReadonlyProperty<T>, IValueSetter<T>
    {
        T Value { get; set; }
    }

    public interface IReadonlyProperty<T> : IValueGetter<T>, IValueNotifier<T>
    {
    }

    public interface IValueGetter<out T>
    {
        T Value { get; }
    }

    public interface IValueSetter<in T>
    {
        T Value { set; }
    }

    public interface IValueNotifier<out T>
    {
        event Action<T> OnValueChanged;
    }
}