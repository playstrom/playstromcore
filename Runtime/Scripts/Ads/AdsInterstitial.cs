using System;
using UnityEngine;

namespace Playstrom.Core.Ads
{
    public abstract class AdsInterstitial : IAdsHandler
    {
        public abstract bool IsAllow();
        
        public virtual bool TryShowAd()
        {
            if (!IsAllow())
            {
                Debug.LogWarning("Interstitial is not allowed");
                return false;
            }

            ShowInterstitial(m_key, HandleWatchingAds);
            return true;
        }
        
        protected AdsInterstitial(string key, Action onCloseCallback = null, object data = null)
        {
            m_data = data;
            m_key = key;
            m_onCloseCallback = onCloseCallback;
        }

        protected abstract void ShowInterstitial(string key, Action onCloseCallback = null);
        
        private void HandleWatchingAds()
        {
            m_onCloseCallback?.Invoke();
        }

        protected object m_data;
        protected string m_key;
        private Action m_onCloseCallback;
    }
}