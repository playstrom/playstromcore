using System;
using UnityEngine;

namespace Playstrom.Core.Ads
{
    public abstract class AdsReward : IAdsHandler
    {
        public event Action<bool> OnRewardedVideoCompleted;
        public abstract bool IsAllow();

        protected AdsReward(Action adReward, string key, object data = null)
        {
            m_adReward = adReward;
            m_key = key;
            m_data = data;
        }

        public virtual bool TryShowAd()
        {
            if (!IsAllow())
            {
#if UNITY_EDITOR
                Debug.LogWarning("Ads is not allowed");
#endif
                return false;
            }

            ShowRewardedVideo(m_key, HandleWatchingAds);
            return true;
        }

        protected abstract void ShowRewardedVideo(string key, Action<bool> handler);

        private void HandleWatchingAds(bool isComplete)
        {
            if (!isComplete)
            {
                OnRewardedVideoCompleted?.Invoke(false);
                return;
            }

            m_adReward?.Invoke();
            OnRewardedVideoCompleted?.Invoke(true);
        }

        protected object m_data;
        protected string m_key;
        private Action m_adReward;
    }
}