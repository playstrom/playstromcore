using System;

namespace Playstrom.Core.Ads
{
    public interface IAdsHandler
    {
        bool TryShowAd();
        bool IsAllow();
    }
}