﻿using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Playstrom.Core.Effects.CollectEffect
{
    public class CollectItem : MonoBehaviour
    {
        public bool IsPlaying { get; set; }
        public float DelayTime => m_delayTime;
        public float DelayStartTime => m_delayStartTime;

        public class NewInfo
        {
            public Sprite Sprite { get; }
            public Mesh Mesh { get; }
            public Material Material { get; }

            public NewInfo(Sprite newSprite, Mesh newMesh, Material newMaterial)
            {
                Sprite = newSprite;
                Mesh = newMesh;
                Material = newMaterial;
            }
        }

        private struct BezierData
        {
            public Vector3 startPoint;
            public Vector3 addPoint;
            public Vector3 finishPoint;
        }
        
        public void SetInfo(CollectTypeFly currentTypeFly, GameObject from, GameObject to, CollectConfig collectConfig = null, NewInfo newInfo = null)
        {
            m_currentTypeFly = currentTypeFly;
            if (newInfo != null)
                switch (m_typeItem)
                {
                    case CollectTypeItem.World:
                        if (newInfo.Mesh != null)
                            m_meshFilter.mesh = newInfo.Mesh;
                        if (newInfo.Material != null)
                            m_meshRenderer.sharedMaterial = newInfo.Material;
                        break;
                    case CollectTypeItem.Ui:
                        if (newInfo.Sprite != null)
                            m_image.sprite = newInfo.Sprite;
                        break;
                }

            m_currConfig = collectConfig == null ? m_defaultConfig : collectConfig;
            m_from = from;
            m_to = to;
            CashSomeValues();
        }

        private void CashSomeValues()
        {
            m_delayTime = Random.Range(m_currConfig.DelayTime.MinValue, m_currConfig.DelayTime.MaxValue);
            m_delayStartTime = Random.Range(m_currConfig.DelayStartTime.MinValue, m_currConfig.DelayStartTime.MaxValue);
            
            var rotate = Random.Range(m_currConfig.Rotate.MinValue, m_currConfig.Rotate.MaxValue);
            Vector3 addRotate = m_typeItem == CollectTypeItem.Ui ? Vector3.forward : Vector3.one;
            m_rotateBezierData.startPoint = m_from.transform.eulerAngles;
            m_rotateBezierData.addPoint = addRotate * rotate;
            m_rotateBezierData.finishPoint = m_to.transform.eulerAngles;
            
            m_randomPerpPos = Random.Range(m_currConfig.PosPerp.MinValue, m_currConfig.PosPerp.MaxValue);
            m_randomPerpLenght = Random.Range(m_currConfig.LenghtPerp.MinValue, m_currConfig.LenghtPerp.MaxValue);
            
            m_camera = Camera.main;
            if (m_camera == null) return;
            switch (m_currConfig.PathDir)
            {
                case CollectConfig.PathDirection.Right:
                    m_direction = m_camera.transform.right;
                    break;
                case CollectConfig.PathDirection.Left:
                    m_direction = -m_camera.transform.right;
                    break;
                case CollectConfig.PathDirection.Up:
                    m_direction = m_camera.transform.up;
                    break;
                case CollectConfig.PathDirection.Down:
                    m_direction = -m_camera.transform.up;
                    break;
                case CollectConfig.PathDirection.Forward:
                    m_direction = m_camera.transform.forward;
                    break;
                case CollectConfig.PathDirection.Back:
                    m_direction = -m_camera.transform.forward;
                    break;
                case CollectConfig.PathDirection.ForwardAndBack:
                    m_direction = GetRandomPlusMinusValue(m_camera.transform.forward);
                    break;
                case CollectConfig.PathDirection.UpAndDown:
                    m_direction = GetRandomPlusMinusValue(m_camera.transform.up);
                    break;
                case CollectConfig.PathDirection.LeftAndRight:
                    m_direction = GetRandomPlusMinusValue(m_camera.transform.right);
                    break;
            }

            Vector3 GetRandomPlusMinusValue(Vector3 dir)
            {
                if (Random.value > .5f)
                    dir = -dir;
                return dir;
            }
            
            m_fromPosition = m_from.transform.position;
            if (m_currentTypeFly == CollectTypeFly.WorldToUi)
                m_fromPosition = m_camera.transform.InverseTransformPoint(m_fromPosition);
        }

        public void SetState(float progress)
        {
            SetPosition(progress);
            SetScale(progress);
            SetRotate(progress);
        }

        public bool ObjectDestroyed()
        {
            if (m_to == null)
            {
                Debug.LogWarning($" endObject Was destroyed in animation");
                return true;
            }
            if (m_from == null)
            {
                Debug.LogWarning($" fromObject Was destroyed in animation");
                return true;
            }

            return false;
        }

        private void SetScale(float progress)
        {
            float value = m_currConfig.ScaleProgress.Evaluate(progress) * m_currConfig.DefaultScale;
            transform.localScale = new Vector3(value, value, m_typeItem == CollectTypeItem.Ui ? 1 : value);
        }

        private void SetPosition(float progress)
        {
            Vector3 position = CalculatePath(progress);
            // Vector3 position = Bezier(m_posBezierData.startPoint, m_posBezierData.addPoint, m_posBezierData.finishPoint,
            //     m_config.ProgressCurve.Evaluate(progress));
            if (m_currentTypeFly == CollectTypeFly.WorldToUi)
                transform.localPosition = position;
            else transform.position = position;
        }

        private void SetRotate(float progress)
        {
            Vector3 rotation = Vector3.Lerp(m_rotateBezierData.startPoint, m_rotateBezierData.addPoint, progress);

            if (!m_currConfig.NeedRotate) return;
            transform.eulerAngles = rotation;
        }

        private Vector3 Bezier(Vector3 a, Vector3 b, Vector3 c, float t)
        {
            Vector3 ab = Vector3.Lerp(a, b, t);
            Vector3 bc = Vector3.Lerp(b, c, t);
            return Vector3.Lerp(ab, bc, t);
        }

        private Vector3 Perpendicular(Vector3 fromPoint, Vector3 targetPoint)
        {
            Vector3 delta = targetPoint - fromPoint;
            Vector3 randomDotOnVector = fromPoint + delta * m_randomPerpPos;

            Vector3 perpendicular = Vector3.zero;
            switch (m_typeItem)
            {
                case CollectTypeItem.World:
                    perpendicular = m_direction * delta.magnitude;
                    break;
                case CollectTypeItem.Ui:
                    perpendicular = new Vector3(Vector2.Perpendicular(delta).x, Vector2.Perpendicular(delta).y, 0);
                    break;
            }
            return (randomDotOnVector + perpendicular * m_randomPerpLenght);
        }

        private Vector3 CalculatePath(float progress)
        {
            Vector3 toPosition = Vector3.zero;

            switch (m_currentTypeFly)
            {
                case CollectTypeFly.WorldToWorld:
                case CollectTypeFly.UiToUi:
                    toPosition = m_to.transform.position;
                    break;
                case CollectTypeFly.WorldToUi:
                    toPosition = m_to.transform.position;
                    toPosition = m_camera.transform.InverseTransformPoint(toPosition);
                    break;
            }

            Vector3 addPoint = Perpendicular(m_fromPosition, toPosition);
            m_posBezierData.startPoint = m_fromPosition;
            m_posBezierData.addPoint = addPoint;
            m_posBezierData.finishPoint = toPosition;

            return Bezier(m_posBezierData.startPoint, m_posBezierData.addPoint, m_posBezierData.finishPoint,
                m_currConfig.ProgressCurve.Evaluate(progress));
        }

        [SerializeField] private CollectTypeItem m_typeItem = CollectTypeItem.Ui;
        [SerializeField] private CollectConfig m_defaultConfig = null;

        [SerializeField] private Image m_image = null;
        [SerializeField] private MeshFilter m_meshFilter = null;
        [SerializeField] private MeshRenderer m_meshRenderer = null;

        private Camera m_camera;
        private CollectTypeFly m_currentTypeFly;
        private GameObject m_from;
        private Vector3 m_fromPosition;
        private GameObject m_to;
        
        private Vector3 m_direction;
        private float m_randomPerpPos;
        private float m_randomPerpLenght;
        
        private BezierData m_posBezierData;
        private BezierData m_rotateBezierData;
        
        private CollectConfig m_currConfig = null;
        private float m_delayTime = 0f;
        private float m_delayStartTime = 0f;
    }
}