﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;


namespace Playstrom.Core.Effects.CollectEffect
{
    public class PoolCollectElements : MonoBehaviour
    {
        [Serializable]
        private class PoolCollectProtoInfo
        {
            public CollectItem ProtoElement => m_protoElement;
            public Transform Parent => m_parent;
            public int MaxCount => m_maxCount;
            public int StartCount => m_startCount;

            public List<CollectItem> PoolObj
            {
                get => m_poolObj;
                set => m_poolObj = value;
            }

            public List<CollectItem> OutPool
            {
                get => m_outPool;
                set => m_outPool = value;
            }

            [SerializeField] private CollectItem m_protoElement = null;
            [SerializeField] private Transform m_parent = null;
            [SerializeField] private int m_maxCount = 30;
            [SerializeField] private int m_startCount = 30;
            
            private List<CollectItem> m_poolObj = new List<CollectItem>();
            private List<CollectItem> m_outPool = new List<CollectItem>();
        }

        [Inject]
        public void CreateStartElements()
        {
            CreateItems(CollectTypeItem.Ui);
            CreateItems(CollectTypeItem.World);
        }

        public void ReturnToPool(CollectItem cI, CollectTypeItem typeItem)
        {
            PoolCollectProtoInfo proto = typeItem == CollectTypeItem.Ui ? m_protoUiInfo : m_protoWorldInfo;
            var poolOut = proto.OutPool;
            
            cI.gameObject.SetActive(false);
            cI.transform.SetParent(proto.Parent, false);
            if (poolOut.Contains(cI))
            {
                poolOut.Remove(cI);
            }
        }

        public CollectItem GetObjFromPool(CollectTypeItem typeItem, Transform newParent = null)
        {
            PoolCollectProtoInfo proto = typeItem == CollectTypeItem.Ui ? m_protoUiInfo : m_protoWorldInfo;
            var pool = proto.PoolObj;
            var poolOut = proto.OutPool;
            
            CollectItem collectItem = pool.FirstOrDefault(cI => !cI.IsPlaying);
            if (collectItem == null && pool.Count >= proto.MaxCount)
                collectItem = pool.First();
            else if (collectItem != null && !collectItem.IsPlaying)
            {
            }
            else
            {
                collectItem = CreateItem(proto);
                pool.Add(collectItem);
            }

            if (newParent != null)
                collectItem.transform.SetParent(newParent);
            poolOut.Add(collectItem);
            return collectItem;
        }

        private void CreateItems(CollectTypeItem typeItem)
        {
            PoolCollectProtoInfo proto = typeItem == CollectTypeItem.Ui ? m_protoUiInfo : m_protoWorldInfo;
            CollectItem obj = proto.ProtoElement;
            if (obj == null) return; 
            
            int startCount = proto.StartCount;
            for (int i = 0; i < startCount; i++)
            {
                CreateItem(proto);
            }
        }
        
        private CollectItem CreateItem(PoolCollectProtoInfo protoInfo)
        {
            CollectItem cI = Instantiate(protoInfo.ProtoElement, protoInfo.Parent);
            protoInfo.PoolObj.Add(cI);
            cI.gameObject.SetActive(false);
            return cI;
        }

        
        [SerializeField] private PoolCollectProtoInfo m_protoUiInfo = null;
        [SerializeField] private PoolCollectProtoInfo m_protoWorldInfo = null;
    }
}