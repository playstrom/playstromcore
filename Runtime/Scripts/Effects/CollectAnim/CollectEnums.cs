﻿namespace Playstrom.Core.Effects.CollectEffect
{
    public enum CollectTypeItem
    {
        World,
        Ui
    }
    
    public enum CollectTypeFly
    {
        WorldToWorld,
        UiToUi,
        WorldToUi
    }
}