using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

namespace Playstrom.Core.Effects.CollectEffect
{
    [RequireComponent(typeof(ParticleSystem))]
    public class CollectEffect : MonoBehaviour
    {
        [SerializeField] private Transform m_endPointFlying;
        [SerializeField] private float m_delayFlying = 0.3f;
        [SerializeField] private float m_speedMove = 1;
        [SerializeField] private float m_endSize = 0;
        [SerializeField] private float m_endRotation;
        [SerializeField] private float m_delayOnComplete;
        [SerializeField] private AnimationCurve m_curvePosition = AnimationCurve.Linear(0, 0, 1, 1);
        [SerializeField] private AnimationCurve m_curveSize = AnimationCurve.Linear(0, 0, 1, 1);

        private ParticleSystem.Particle[] m_particles;
        private Camera m_camera;
        private Coroutine m_corEffects;
        private ParticleSystem m_currencyParticle = null;
        private ParticleSystemRenderer m_currencyParticleRenderer = null;

        public bool IsPlaying { get; private set; }
        
        private ParticleSystem CurrencyParticle
        {
            get
            {
                if (m_currencyParticle == null)
                    m_currencyParticle = GetComponent<ParticleSystem>();
                return m_currencyParticle;
            }
        }
        
        private ParticleSystemRenderer CurrencyParticleRenderer
        {
            get
            {
                if (m_currencyParticleRenderer == null)
                    m_currencyParticleRenderer = CurrencyParticle.GetComponent<ParticleSystemRenderer>();
                return m_currencyParticleRenderer;
            }
        }
        
        private Camera MainCamera
        {
            get
            {
                if (m_camera == null)
                    m_camera = Camera.main;
                return m_camera;
            }
        }

        public void MoveItemLerp(int reward = 0, Transform endPoint = null, Action onComplete = null,
            Action onFirstCompleted = null, Action<int> onReturnPartItem = null)
        {
            if (endPoint != null) m_endPointFlying = endPoint;
            m_endPointFlying = endPoint;
            CurrencyParticle.Play();
            m_corEffects = StartCoroutine(MoveItemByLerp(reward, onComplete, onFirstCompleted, onReturnPartItem));
        }

        public void StopEffects()
        {
            CurrencyParticle.Stop();
            CurrencyParticle.Clear();
            if (m_corEffects != null)
                StopCoroutine(m_corEffects);
        }

        private IEnumerator MoveItemByLerp(int reward = 0,Action onComplete = null,
            Action onFirstCompleted = null, Action<int> onReturnPartItem = null)
        {
            IsPlaying = true;
            yield return new WaitForSeconds(m_delayFlying);
            m_particles = new ParticleSystem.Particle[CurrencyParticle.main.maxParticles];
            int num = CurrencyParticle.GetParticles(m_particles);

            var listId = new List<int>();
            int numComplete = 0;
            CurrencyParticleRenderer.velocityScale = 0;
            int tempCostForString = reward;
            int temp = num != 0 ? reward % num : 0;
            bool isFirstCompleted = true;

            var distancesPoints = new List<float>();
            var staticPos = new Vector3[num];

            for (int i = 0; i < num; i++)
            {
                distancesPoints.Add(Vector3.Distance(m_particles[i].position, m_endPointFlying.position) *
                                    Random.Range(.6f, 1));
                staticPos[i] = m_particles[i].position;
            }

            float[] timeSpeed = new float[num];

            float size = m_particles[0].startSize;
            while (numComplete != num)
            {
                Vector3 v = MainCamera.WorldToViewportPoint(m_endPointFlying.position);
                Vector3 p = MainCamera.ViewportToWorldPoint(v);
                for (int i = 0; i < num; i++)
                {
                    if (timeSpeed[i] < 0.9f)
                    {
                        timeSpeed[i] += Time.deltaTime * (m_speedMove / distancesPoints[i]);

                        m_particles[i].velocity = Vector3.zero;
                        m_particles[i].position = Vector3.Lerp(staticPos[i], p
                            , m_curvePosition.Evaluate(timeSpeed[i]));
                        m_particles[i].startSize = Mathf.Lerp(size, m_endSize,
                            m_curveSize.Evaluate(timeSpeed[i]));
                        m_particles[i].rotation = Mathf.MoveTowards(m_particles[i].rotation, m_endRotation,
                            m_curvePosition.Evaluate(timeSpeed[i]) * Time.deltaTime);
                    }
                    else
                    {
                        m_particles[i].remainingLifetime = 0;
                        bool isSuccess = true;
                        foreach (int t in listId.Where(t => t == i))
                        {
                            isSuccess = false;
                        }

                        if (!isSuccess) continue;
                        listId.Add(i);
                        numComplete++;

                        onReturnPartItem?.Invoke(reward / num);
                        if (!isFirstCompleted) continue;
                        onFirstCompleted?.Invoke();
                        isFirstCompleted = false;
                    }
                }

                CurrencyParticle.SetParticles(m_particles, num);
                tempCostForString = (int) Mathf.MoveTowards(tempCostForString, 0,
                    (float) reward / CurrencyParticle.main.maxParticles);

                yield return new WaitForEndOfFrame();
            }

            while (tempCostForString != 0)
            {
                tempCostForString = (int) Mathf.MoveTowards(tempCostForString, 0,
                    (float) reward / CurrencyParticle.main.maxParticles);
            }

            onReturnPartItem?.Invoke(temp);

            CurrencyParticle.Stop();
            CurrencyParticle.Clear();
            yield return new WaitForSeconds(m_delayOnComplete);

            onComplete?.Invoke();
            IsPlaying = false;
        }
    }
}