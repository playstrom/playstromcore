﻿using System;
using UnityEngine;

namespace Playstrom.Core.Effects.CollectEffect
{
    [CreateAssetMenu(menuName = "Effects/Collect", fileName = "collect_anim_config")]
    public class CollectConfig : ScriptableObject
    {
        [Serializable]
        public class MinMaxValue
        {
            public MinMaxValue(float minValue = 0, float maxValue = 0)
            {
                m_minValue = minValue;
                m_maxValue = maxValue;
            }

            public float MinValue => m_minValue;
            public float MaxValue => m_maxValue;

            [SerializeField] private float m_minValue = 0;
            [SerializeField] private float m_maxValue = 0;
        }

        public enum PathDirection
        {
            Left,
            Right,
            LeftAndRight,
            Up,
            Down,
            UpAndDown,
            Forward,
            Back,
            ForwardAndBack
        }

        public MinMaxValue DelayTime => m_delayTime;
        public MinMaxValue DelayStartTime => m_delayStartTime;

        public MinMaxValue PosPerp => m_posPerp;
        public MinMaxValue LenghtPerp => m_lenghtPerp;
        public PathDirection PathDir => m_pathDirection;

        public AnimationCurve ScaleProgress => m_scaleProgress;
        public float DefaultScale => m_defaultScale;
        public AnimationCurve ProgressCurve => m_progressCurve;
        public bool NeedRotate => m_needRotate;
        public MinMaxValue Rotate => m_rotate;



        [Space] [Header("Time")] 
        [SerializeField] private MinMaxValue m_delayTime = new MinMaxValue(0.9f, 1.4f);
        [SerializeField] private MinMaxValue m_delayStartTime = new MinMaxValue(0f, 0.3f);

        [Space] [Header("Perpendicular")] 
        [SerializeField] private MinMaxValue m_posPerp = new MinMaxValue(0.25f, 0.75f);
        [SerializeField] private MinMaxValue m_lenghtPerp = new MinMaxValue(0f, 0.2f);
        [SerializeField] private PathDirection m_pathDirection = PathDirection.LeftAndRight;

        [Space] [Header("Progress state")]
        [SerializeField] private AnimationCurve m_scaleProgress = null;
        [SerializeField] private float m_defaultScale = 1;
        [SerializeField] private AnimationCurve m_progressCurve = null;
        [SerializeField] private bool m_needRotate = false;
        [SerializeField] private MinMaxValue m_rotate = new MinMaxValue(0f, 1080f);
    }
}
