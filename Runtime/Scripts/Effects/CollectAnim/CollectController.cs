﻿using System;
using System.Collections;
using UnityEngine;
using Zenject;

namespace Playstrom.Core.Effects.CollectEffect
{
    public class CollectController : MonoBehaviour
    {
        public void ResourceFlight(CollectTypeFly typeFly, GameObject from, GameObject to, long itemCount, CollectConfig collectConfig = null,
            CollectItem.NewInfo newInfo = null, Action<float> onAnimProgress = null)
        {
            m_onAnimProgress = onAnimProgress;
            m_objCount = itemCount;
            m_currentTypeFly = typeFly;
            MoveToTarget(from, to, collectConfig, newInfo != null ? new CollectItem.NewInfo(newInfo.Sprite, newInfo.Mesh, newInfo.Material) : null);
        }

        private void MoveToTarget(GameObject from, GameObject to, CollectConfig collectConfig = null, CollectItem.NewInfo newInfo = null)
        {
            m_animCount = 0;
            m_startAnimCount = m_objCount;
            for (int i = 0; i < m_objCount; i++)
            {
                CollectItem cI = GetObjFromPool();
                cI.SetInfo(m_currentTypeFly, from, to, collectConfig,
                    newInfo != null ? new CollectItem.NewInfo(newInfo.Sprite, newInfo.Mesh, newInfo.Material) : null);
                m_animCount++;

                StartCoroutine(MakeProgress(cI));
            }
        }

        private IEnumerator MakeProgress(CollectItem cI)
        {
            yield return new WaitForSeconds(cI.DelayStartTime);
            cI.SetState(0);
            cI.gameObject.SetActive(true);

            float elapsedTime = 0;
            while (elapsedTime < cI.DelayTime)
            {
                if (cI.ObjectDestroyed())
                {
                    ReturnToPool(cI);
                    m_onAnimProgress?.Invoke(0);
                    yield break;
                }
                var progress = elapsedTime / cI.DelayTime;
                cI.SetState(progress);

                elapsedTime += Time.deltaTime;
                yield return null;
            }
            
            if (cI.ObjectDestroyed())
            {
                ReturnToPool(cI);
                m_onAnimProgress?.Invoke(0);
                yield break;
            }

            cI.SetState(1);
            ReturnToPool(cI);

            --m_animCount;
            var step = 1f - ((float) m_animCount / m_startAnimCount);
            m_onAnimProgress?.Invoke(step);
            yield return null;
        }

        private CollectItem GetObjFromPool()
        {
            CollectItem collectItem = null;
            switch (m_currentTypeFly)
            {
                case CollectTypeFly.WorldToWorld:
                    collectItem = m_pool.GetObjFromPool(CollectTypeItem.World);
                    break;
                case CollectTypeFly.WorldToUi:
                    collectItem = m_pool.GetObjFromPool(CollectTypeItem.World, Camera.main.transform);
                    break;
                case CollectTypeFly.UiToUi:
                    collectItem = m_pool.GetObjFromPool(CollectTypeItem.Ui);
                    break;
            }

            if (collectItem != null)
                collectItem.IsPlaying = true;
            return collectItem;
        }

        private void ReturnToPool(CollectItem collectItem)
        {
            collectItem.IsPlaying = false;
            switch (m_currentTypeFly)
            {
                case CollectTypeFly.WorldToWorld:
                case CollectTypeFly.WorldToUi:
                    m_pool.ReturnToPool(collectItem, CollectTypeItem.World);
                    break;
                case CollectTypeFly.UiToUi:
                    m_pool.ReturnToPool(collectItem, CollectTypeItem.Ui);
                    break;
            }
        }


        [Inject] private PoolCollectElements m_pool;

        private int m_animCount;
        private long m_startAnimCount;
        private long m_objCount;
        private Action<float> m_onAnimProgress;
        private CollectTypeFly m_currentTypeFly = CollectTypeFly.UiToUi;
    }
}