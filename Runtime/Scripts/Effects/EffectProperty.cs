﻿using System;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    [Serializable]
    public class EffectProperty
    {
        [SerializeField] private string m_effectName = null;
        [SerializeField] private bool m_isStatic = false;
        [SerializeField] private int m_staticCount = 0;
        [SerializeField] private int m_maxCount = 1;

        public string EffectName => m_effectName;
        public bool IsStatic => m_isStatic;
        public int StaticCount => m_staticCount;
        public int MaxCount => m_maxCount;
        
        public EffectProperty(string effectName, int maxCount = 1)
        {
            m_effectName = effectName;
            m_maxCount = maxCount;
        }
    }
}