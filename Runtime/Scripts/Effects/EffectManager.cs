﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Playstrom.Core.Effects
{
    public class EffectManager
    {
        [Inject] private Effects m_effects = null;

        [Inject]
        public void Init()
        {
            m_effects.CreateStaticEffects();
        }

        public EffectPlayer PlayEffect(string name, Transform parent = null, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion())
        {
            var effect = m_effects.GetEffectFromPool(name, out List<EffectPlayer> group);
            effect.Play(parent, position, rotation);
            m_effects.AddToPlay(group, effect);
            return effect;
        }

        public void StopAllActiveEffects()
        {
            m_effects.StopActiveEffects();
        }
    }
}