﻿using UnityEditor;

namespace Playstrom.Core.Effects.CollectEffect
{
    [CustomEditor(typeof(CollectItem))]
    [CanEditMultipleObjects]
    public class CollectItemEditor : Editor
    {
        SerializedProperty m_typeItem = null;
        SerializedProperty m_config = null;

        SerializedProperty m_image = null;
        SerializedProperty m_meshFilter = null;
        SerializedProperty m_meshRenderer = null;

        protected void OnEnable()
        {
            m_typeItem = serializedObject.FindProperty("m_typeItem");
            m_config = serializedObject.FindProperty("m_defaultConfig");
            
            m_image = serializedObject.FindProperty("m_image");
            m_meshFilter = serializedObject.FindProperty("m_meshFilter");
            m_meshRenderer = serializedObject.FindProperty("m_meshRenderer");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(m_typeItem);
            EditorGUILayout.PropertyField(m_config);
            
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Info", EditorStyles.boldLabel);
            switch (m_typeItem.enumValueIndex)
            {
                case 0:
                    EditorGUILayout.PropertyField(m_meshFilter);
                    EditorGUILayout.PropertyField(m_meshRenderer);
                    break;
                case 1:
                    EditorGUILayout.PropertyField(m_image);
                    break;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
