﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    [CustomEditor(typeof(Effects))]
    public class EffectsEditor : Editor
    {
        List<EffectProperty> m_effectProperties = new List<EffectProperty>();
        List<GameObject> m_effectsGameObjs = new List<GameObject>();
        private Effects Target { get; set; }
        
        private void OnEnable()
        {
            Target = (Effects)target;
        }

        private void OnDisable()
        {
            Target = null;
        }

        private bool FoundMatchProp(EffectProperty effectProperty)
        {
            return m_effectsGameObjs.Any(effectGameObj => effectGameObj.name == effectProperty.EffectName);
        }
        
        private bool FoundMatchGameObj(GameObject effectGameObj)
        {
            return m_effectProperties.Any(effProp => effProp.EffectName == effectGameObj.name);
        }

        private void DeleteNotFounded()
        {
            m_effectProperties.RemoveAll(effProp => !FoundMatchProp(effProp));
        }

        private void AutoFill()
        {
            m_effectProperties = Target.EffectsProperties;
            var path = serializedObject.FindProperty("m_effectsPath").stringValue;
            m_effectsGameObjs = Resources.LoadAll<GameObject>(path).ToList();
            DeleteNotFounded();
            foreach (var effGameObj in m_effectsGameObjs)
            {
                if(FoundMatchGameObj(effGameObj)) continue;
                m_effectProperties.Add(new EffectProperty(effGameObj.name));
            }
        }

        private int SortByName(EffectProperty x, EffectProperty y)
        {
            return string.Compare(x.EffectName, y.EffectName, StringComparison.Ordinal);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Auto fill"))
            {
                AutoFill();
            }

            if (GUILayout.Button("Sorted by name"))
            {
                m_effectProperties.Sort(SortByName);
            }
            
            if (GUI.changed)
            {
                EditorUtility.SetDirty(Target);
            }
        }
    }
}
