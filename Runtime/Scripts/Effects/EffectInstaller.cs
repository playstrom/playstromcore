using Playstrom.Core.Effects;
using UnityEngine;
using Zenject;

public class EffectInstaller : MonoInstaller
{
    [SerializeField] private Effects effects;
    
    public override void InstallBindings()
    {
        Container.Bind<Effects>().FromComponentInNewPrefab(effects).AsCached();
        Container.Bind<EffectManager>().AsCached().NonLazy();
    }
    #if UNITY_EDITOR
    public void SetEffects(Effects effects)
    {
        this.effects = effects;
    }
    #endif
}