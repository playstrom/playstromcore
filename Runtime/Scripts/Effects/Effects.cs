﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    public class Effects : MonoBehaviour
    {
        [SerializeField] private string m_effectsPath = "Prefabs/Effects/";
        [SerializeField] private List<EffectProperty> m_effectsProperties = null;

        private Dictionary<EffectProperty, List<EffectPlayer>> m_createdEffects =
            new Dictionary<EffectProperty, List<EffectPlayer>>();

        private Dictionary<EffectProperty, GameObject> m_effectsPrefab = new Dictionary<EffectProperty, GameObject>();

        private List<EffectPlayer> m_activeEffects = new List<EffectPlayer>();
        private Coroutine m_waitEndEffects = null;

        public List<EffectProperty> EffectsProperties
        {
            get => m_effectsProperties;
            set => m_effectsProperties = value;
        }

        private void Update()
        {
            // if (m_activeEffects.Count == 0) return;
            // m_activeEffects.RemoveAll(eff => !eff.IsPlaying);
            WaitEndOfEffects();
        }

        private GameObject LoadEffectPrefab(EffectProperty effectProperty)
        {
            string path = m_effectsPath + effectProperty.EffectName;
            GameObject effectPrefab = Resources.Load<GameObject>(path);
            if (effectPrefab == null) Debug.LogWarning("Effect prefab not found");
            return effectPrefab;
        }

        private EffectPlayer CreateEffect(GameObject effectPrefab)
        {
            GameObject gObj = Instantiate(effectPrefab, transform);
            gObj.SetActive(false);
            EffectPlayer effect = gObj.GetComponent<EffectPlayer>();
            effect.EndEffect += OnEffectEnded;
            effect.DestroyEffect += OnEffectDestroyed;
            return effect;
        }

        private EffectProperty FindCreatedEffectProperty(string targetName)
        {
            return m_createdEffects.FirstOrDefault(eff => eff.Key.EffectName == targetName).Key;
        }

        private List<EffectPlayer> FindEffectsFromProp(EffectProperty effectProperty)
        {
            foreach (var crEff in m_createdEffects)
            {
                if (crEff.Key == effectProperty) return crEff.Value;
            }

            return null;
        }

        private void OnEffectEnded(EffectPlayer effectPlayer)
        {
            effectPlayer.gameObject.SetActive(false);
            effectPlayer.transform.SetParent(transform, false);
            if (m_activeEffects.Contains(effectPlayer)) m_activeEffects.Remove(effectPlayer);
        }

        private void OnEffectDestroyed(EffectPlayer effectPlayer)
        {
            effectPlayer.EndEffect -= OnEffectEnded;
            effectPlayer.DestroyEffect -= OnEffectDestroyed;
            if (m_activeEffects.Contains(effectPlayer)) m_activeEffects.Remove(effectPlayer);

            foreach (var crEffect in m_createdEffects)
            {
                crEffect.Value.Remove(effectPlayer);
            }
        }

        private void WaitEndOfEffects()
        {
            if (m_activeEffects.Count == 0) return;
            if (m_activeEffects.All(actEff => actEff.IsPlaying)) return;

            var all = m_activeEffects.Where(activeEffect => !activeEffect.IsPlaying).ToList();
            foreach (var activeEffect in all)
            {
                activeEffect.Stop();
            }
        }

        private GameObject GetOrLoadPrefab(EffectProperty effectProperty)
        {
            if (m_effectsPrefab.TryGetValue(effectProperty, out GameObject prefab)) return prefab;
            prefab = LoadEffectPrefab(effectProperty);
            m_effectsPrefab.Add(effectProperty, prefab);

            return prefab;
        }

        public void AddToPlay(List<EffectPlayer> effects, EffectPlayer effectPlayer)
        {
            m_activeEffects.Add(effectPlayer);
            effects.Remove(effectPlayer);
            effects.Add(effectPlayer);
        }

        public void StopActiveEffects()
        {
            foreach (var effect in m_activeEffects)
            {
                effect.Stop();
            }
        }

        public void CreateStaticEffects()
        {
            foreach (var effectProperty in m_effectsProperties)
            {
                if (!effectProperty.IsStatic) continue;
                GameObject prefab = GetOrLoadPrefab(effectProperty);

                var effects = new List<EffectPlayer>();
                for (int i = 0; i < effectProperty.StaticCount; i++)
                {
                    effects.Add(CreateEffect(prefab));
                }

                m_createdEffects.Add(effectProperty, effects);
            }
        }

        public EffectPlayer GetEffectFromPool(string targetName, out List<EffectPlayer> effects)
        {
            EffectProperty effectProperty = FindCreatedEffectProperty(targetName);
            EffectPlayer effect = null;

            if (effectProperty == null)
            {
                effectProperty = m_effectsProperties.Find(effProp => effProp.EffectName == targetName);
                GameObject prefab = GetOrLoadPrefab(effectProperty);
                effect = CreateEffect(prefab);
                effects = new List<EffectPlayer> {effect};
                m_createdEffects.Add(effectProperty, effects);
                effect = effects.First();
            }
            else
            {
                effects = FindEffectsFromProp(effectProperty);

                effect = effects.FirstOrDefault(eff => !eff.IsPlaying);
                if (effect == null && effects.Count >= effectProperty.MaxCount)
                {
                    effect = effects.First();
                }

                if (effect != null)
                {
                    if (effect.IsPlaying) effect.Stop();
                    return effect;
                }

                effect = CreateEffect(m_effectsPrefab[effectProperty]);
                m_createdEffects[effectProperty].Add(effect);
            }

            return effect;
        }
    }
}