﻿using System;
using System.Collections;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    [Serializable]
    [RequireComponent(typeof(AudioSource))]
    public class AudioPlayer : EffectPlayer
    {
        [SerializeField] private AudioSource m_audioSource = null;
        [SerializeField] private float m_fadeTime = 1.5f;
        [SerializeField] private bool m_fadeRealTime = false;

        private float m_startVolume = 1f;
        private float m_endVolume = 0f;
        private bool m_withStop = false;

        private float GetTime()
        {
            return m_fadeRealTime ? Time.realtimeSinceStartup : Time.time;
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            if (m_audioSource == null) m_audioSource = GetComponent<AudioSource>();
        }

        protected override IEnumerator FadeCoroutine()
        {
            var startVolume = m_audioSource.volume;
            var timeFromStart = m_audioSource.time;
            var elapsedTime = 0f;
            while (Math.Abs(elapsedTime - m_fadeTime) > 0.01f)
            {
                float progress = timeFromStart / m_fadeTime;
                m_audioSource.volume = Mathf.Lerp(startVolume, m_endVolume, progress);
                elapsedTime += GetTime();
                yield return null;
            }

            m_audioSource.volume = m_endVolume;
            if (m_withStop) Stop();
        }

        public override void Play(Transform parent = null, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion())
        {
            base.Play(parent, position, rotation);
            m_audioSource.volume = m_startVolume;
            m_audioSource.Play();
        }

        public override void Stop()
        {
            TryToStopCoroutine();
            m_audioSource.Stop();
            base.Stop();
        }

        public override void Fade()
        {
            Fade( 0, true);
            base.Fade();
        }

        public override bool IsPlaying
        {
            get
            {
                if (!m_audioSource.isActiveAndEnabled) return m_audioSource.isPlaying;
                if (m_audioSource.clip != null && m_audioSource.clip.loadState == AudioDataLoadState.Loading)
                {
                    return true;
                }

                return m_audioSource.isPlaying;
            }
        }

        public override bool IsLooping => m_audioSource.loop;

        public void Fade(float toVolume, bool withStop = false)
        {
            TryToStopCoroutine();
            m_withStop = withStop;
            m_endVolume = toVolume;
        }
    }
}