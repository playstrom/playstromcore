﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    public class CompositePlayer : EffectPlayer
    {
        [SerializeField] private List<EffectPlayer> m_effects = new List<EffectPlayer>();

        protected override void OnValidate()
        {
            base.OnValidate();
            m_effects = GetComponentsInChildren<EffectPlayer>().ToList();
        }
        
        protected override IEnumerator FadeCoroutine()
        {
            while (m_effects.Any(ps => ps.IsPlaying))
            {
                yield return null;
            }

            Stop();
        }

        public override void Play(Transform parent = null, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion())
        {
            base.Play(parent, position, rotation);
            foreach (var effect in m_effects)
            {
                effect.Play(parent, position, rotation);
            }
        }

        public override void Stop()
        {
            TryToStopCoroutine();
            foreach (var effect in m_effects)
            {
                effect.Stop();
            }

            base.Stop();
        }

        public override void Fade()
        {
            TryToStopCoroutine();
            foreach (var effect in m_effects)
            {
                effect.Fade();
            }

            base.Fade();
        }

        public override bool IsPlaying => m_effects.Any(eff => eff.IsPlaying);

        public override bool IsLooping => m_effects.Any(eff => eff.IsLooping);
    }
}
