﻿using System;
using System.Collections;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    public abstract class EffectPlayer : MonoBehaviour
    {
        public event Action<EffectPlayer> EndEffect = null;
        public event Action<EffectPlayer> DestroyEffect = null;
        
        protected Coroutine m_fadeCoroutine = null;
        
        public virtual bool IsPlaying { get; protected set; }
        public virtual bool IsLooping { get; }

        private void OnDestroy()
        {
            DestroyEffect?.Invoke(this);
            Stop();
        }

        protected void TryToStopCoroutine()
        {
            if (m_fadeCoroutine == null) return;
            StopCoroutine(m_fadeCoroutine);
            m_fadeCoroutine = null;
        }
        
        protected virtual void OnValidate() { }

        protected virtual IEnumerator FadeCoroutine()
        {
            yield return null;
        }

        public virtual void Play(Transform parent = null, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion())
        {
            IsPlaying = true;
            if (transform.parent != parent && parent != null) transform.SetParent(parent, false);
            if (transform.localPosition != position) transform.localPosition = position;
            if (transform.localRotation != rotation) transform.localRotation = rotation;
            gameObject.SetActive(true);
        }

        public virtual void Stop()
        {
            EndEffect?.Invoke(this);
            IsPlaying = false;
        }

        public virtual void Fade()
        {
            if (gameObject.activeInHierarchy)
                m_fadeCoroutine = StartCoroutine(FadeCoroutine());
            else
                Stop();
        }
    }
}
