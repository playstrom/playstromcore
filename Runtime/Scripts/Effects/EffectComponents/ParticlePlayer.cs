﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    [Serializable]
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticlePlayer : EffectPlayer
    {
        [SerializeField] private List<ParticleSystem> m_particles = null;

        public List<ParticleSystem> Particles => m_particles;

        private void Start()
        {
            foreach (var particle in m_particles)
            {
                var main = particle.main;
                if (main.stopAction == ParticleSystemStopAction.Disable)
                    main.stopAction = ParticleSystemStopAction.None;
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            if (m_particles == null) m_particles = GetComponentsInChildren<ParticleSystem>().ToList();
        }
        
        protected override IEnumerator FadeCoroutine()
        {
            while (m_particles.Any(ps => ps.isPlaying))
            {
                yield return null;
            }

            Stop();
        }

        public override void Play(Transform parent = null, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion())
        {
            base.Play(parent, position, rotation);
            foreach (var particle in m_particles)
            {
                particle.Play(false);
            }
        }

        public override void Stop()
        {
            TryToStopCoroutine();
            foreach (var particle in m_particles)
            {
                particle.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
            }

            base.Stop();
        }

        public override void Fade()
        {
            TryToStopCoroutine();
            foreach (var particle in m_particles)
            {
                particle.Stop(false, ParticleSystemStopBehavior.StopEmitting);
            }

            base.Fade();
        }

        public override bool IsPlaying => m_particles.Any(ps => ps.isPlaying);
        public override bool IsLooping => m_particles.Any(ps => ps.main.loop);
    }
}
