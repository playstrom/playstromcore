﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    [Serializable]
    public class MultiEffectConfigComponent : MonoBehaviour
    {
        [SerializeField] private List<EffectConfig> m_effects = new List<EffectConfig>();

        public List<EffectPlayer> PlayEffectInPos()
        {
            return m_effects.Select(effect => effect.PlayEffectInPos()).ToList();
        }

        public List<EffectPlayer> PlayEffectInParent()
        {
            return m_effects.Select(effect => effect.PlayEffectInParent()).ToList();
        }

        public void StopEffect()
        {
            foreach (var effect in m_effects)
            {
                effect.StopEffect();
            }
        }

        public void FadeEffect()
        {
            foreach (var effect in m_effects)
            {
                effect.FadeEffect();
            }
        }

        public bool IsPlaying => m_effects.Any(eff => eff.IsPlaying);
    }
}