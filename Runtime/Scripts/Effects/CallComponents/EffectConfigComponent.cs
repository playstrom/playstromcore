﻿using System;
using UnityEngine;

namespace Playstrom.Core.Effects
{
    [Serializable]
    public class EffectConfigComponent : MonoBehaviour
    {
        [SerializeField] private EffectConfig m_effect = null;

        public EffectPlayer PlayEffectInPos()
        {
            return m_effect.PlayEffectInPos();
        }

        public EffectPlayer PlayEffectInParent()
        {
            return m_effect.PlayEffectInParent();
        }

        public void StopEffect()
        {
            m_effect.StopEffect();
        }

        public void FadeEffect()
        {
            m_effect.FadeEffect();
        }

        public bool IsPlaying => m_effect.IsPlaying;
    }
}