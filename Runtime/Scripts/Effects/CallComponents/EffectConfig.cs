﻿using System;
using UnityEngine;
using Zenject;

namespace Playstrom.Core.Effects
{
    [Serializable]
    public class EffectConfig
    {
        [Inject] private EffectManager m_effectManager = null;

        public EffectManager EffectManager =>
            m_effectManager ??
            (m_effectManager = ProjectContext.Instance.Container.Resolve<EffectManager>());

        [SerializeField] private string m_effectName = null;
        [SerializeField] private Transform m_parent = null;

        private EffectPlayer m_currentEffectPlayer = null;
        
        public EffectConfig()
        {
        }

        public EffectConfig(string effectName = null, Transform parent = null)
        {
            m_effectName = effectName;
            m_parent = parent;
        }

        public EffectPlayer PlayEffectInPos()
        {
            if (string.IsNullOrEmpty(m_effectName)) return null;
            StopEffect();
            m_currentEffectPlayer = EffectManager.PlayEffect(m_effectName,null, m_parent.position);
            return m_currentEffectPlayer;
        }

        public EffectPlayer PlayEffectInParent()
        {
            if (string.IsNullOrEmpty(m_effectName)) return null;
            StopEffect();
            m_currentEffectPlayer = EffectManager.PlayEffect(m_effectName, m_parent);
            return m_currentEffectPlayer;
        }

        public void StopEffect()
        {
            if (m_currentEffectPlayer == null) return;
            m_currentEffectPlayer.Stop();
            m_currentEffectPlayer = null;
        }

        public void FadeEffect()
        {
            if (m_currentEffectPlayer == null) return;
            m_currentEffectPlayer.Fade();
            m_currentEffectPlayer = null;
        }

        public bool IsPlaying => (m_currentEffectPlayer != null) && m_currentEffectPlayer.IsPlaying;
    }
}