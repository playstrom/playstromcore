﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Playstrom.Core
{
    public static class SaveSystem<T> where T : class
    {
        private const string Format = ".playstrom";

        public static void SaveData(T data, string name)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/" + name + Format;
            FileStream stream = new FileStream(path, FileMode.Create);
            formatter.Serialize(stream, data);
            stream.Close();
        }


        public static T LoadData(string name)
        {
            string path = Application.persistentDataPath + "/" + name + Format;
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);
                T returnData = formatter.Deserialize(stream) as T;
                stream.Close();
                return returnData;
            }

            Debug.LogError("!!!!!!!!!!! no file found by path: " + path + " !!!!!!!!!!!!!!!!");
            return null;
        }
    }
    
}


