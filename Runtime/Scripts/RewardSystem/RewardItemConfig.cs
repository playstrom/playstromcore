using UnityEngine;
using UnityEngine.Serialization;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.REWARD + Constants.SLASH + Constants.REWARD_ITEM_CONFIG, fileName = Constants.REWARD_ITEM_CONFIG)]
    public class RewardItemConfig:ScriptableObject
    {
        [FormerlySerializedAs("rewardItem")] [SerializeField] private RewardItemBase rewardItemBase;
        [SerializeField] private float weightToDrop;

        public RewardItemBase RewardItemBase => rewardItemBase;

        public float WeightToDrop => weightToDrop;
    }
}