namespace Playstrom.Core
{
    public enum RewardType
    {
        None,
        Soft,
        Hard,
        Special
    }
}