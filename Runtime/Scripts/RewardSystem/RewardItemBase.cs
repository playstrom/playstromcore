using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.REWARD + Constants.SLASH + Constants.REWARD_ITEM_BASE, fileName = Constants.REWARD_ITEM_BASE)]
    public class RewardItemBase: ScriptableObject
    {
        [SerializeField] private RewardType rewardType;
        [SerializeField] private uint count = 1;

        public RewardType RewardType => rewardType;

        public uint Count => count;
    }
}