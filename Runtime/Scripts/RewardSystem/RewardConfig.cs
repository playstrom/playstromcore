using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.REWARD + Constants.SLASH + Constants.REWARD_CONFIG, fileName = Constants.REWARD_CONFIG)]
    public class RewardConfig : ScriptableObject
    {
        [SerializeField] private RewardItemConfig[] rewardItemConfigs;

        public RewardItemConfig[] RewardItemConfigs => rewardItemConfigs;
    }
}