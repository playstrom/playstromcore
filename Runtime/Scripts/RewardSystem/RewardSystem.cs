using System.Linq;
using UnityEngine;

namespace Playstrom.Core
{
    public class RewardSystem
    {
        private RewardItemConfig[] rewardItemConfigs;
        
        public RewardSystem(RewardItemConfig[] rewardItemConfigs)
        {
            this.rewardItemConfigs = rewardItemConfigs;
        }

        public RewardItemBase GetRewardItem()
        {
            CheckWeights();
            RewardItemBase itemBase = null;
            float sum = rewardItemConfigs.Sum(x => x.WeightToDrop);
            float rand = Random.Range(0f, sum);
            float tempSum = 0f;
            for (int i = 0; i < rewardItemConfigs.Length; i++)
            {
                float itemWeight = rewardItemConfigs[i].WeightToDrop + tempSum;
                if (rand < itemWeight)
                {
                    itemBase = rewardItemConfigs[i].RewardItemBase;
                    break;
                }

                tempSum += rewardItemConfigs[i].WeightToDrop;
            }

            if (itemBase == null)
            {
                itemBase = rewardItemConfigs[rewardItemConfigs.Length - 1].RewardItemBase;
                Debug.LogWarning("!!!!!! Was not current found!!!!!!");
            }

            return itemBase;
        }

        public virtual void CheckWeights()
        {
            
        }
    }
}