﻿using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.MANAGERS + Constants.SLASH + Constants.META_MANAGER, fileName = Constants.META_MANAGER)]
    public class MetaManager : BaseManager
    {
        public override void Init()
        {
            
        }
    }
}
