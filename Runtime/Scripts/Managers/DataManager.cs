﻿using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.MANAGERS + Constants.SLASH +  Constants.DATA_MANAGER, fileName = Constants.DATA_MANAGER)]
    public class DataManager : BaseManager
    {
        public override void Init()
        {

        }
    }
}