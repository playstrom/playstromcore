﻿using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.MANAGERS + Constants.SLASH + Constants.EVENT_MANAGER, fileName = Constants.EVENT_MANAGER)]
    public class EventManager : BaseManager
    {
        public override void Init()
        {

        }
    }
}