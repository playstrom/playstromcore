﻿using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.MANAGERS + Constants.SLASH + Constants.UI_MANAGER, fileName = Constants.UI_MANAGER)]
    public class UiManager : BaseManager
    {
        public override void Init()
        {
          
        }
    }
}
