﻿using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.MANAGERS + Constants.SLASH + Constants.GAME_PLAY_MANAGER, fileName = Constants.GAME_PLAY_MANAGER)]
    public class GamePlayManager : BaseManager
    {
        public override void Init()
        {
            
        }
    }
}
