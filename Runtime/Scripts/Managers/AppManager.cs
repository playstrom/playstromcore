﻿using System;
using UnityEditor;
using UnityEngine;

namespace Playstrom.Core
{    
    [CreateAssetMenu(menuName = Constants.MANAGERS + Constants.SLASH + Constants.APP_MANAGER, fileName = Constants.APP_MANAGER)]
    public class AppManager: BaseManager
    {
        private const int Target_Fps = 60;
        
        [SerializeField] private ManagersConfig managersConfig;

        public override void Init()
        {
            Application.targetFrameRate = Target_Fps;
            Application.lowMemory += ApplicationOnLowMemory;
            managersConfig.Init();
        }

        private void ApplicationOnLowMemory()
        {
            Resources.UnloadUnusedAssets();
            GC.Collect();
        }

       
    }

    public class InitHelper: MonoBehaviour
    {
        [RuntimeInitializeOnLoadMethod]
        private static void Init()
        {
            Instantiate(Resources.Load(Constants.APP_MANAGER) as AppManager).Init();
        }
    }
    
}
