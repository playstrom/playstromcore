﻿using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.MANAGERS + Constants.SLASH + Constants.CONFIG_MANAGER, fileName = Constants.CONFIG_MANAGER)]
    public class ConfigManager : BaseManager
    {
        public override void Init()
        {

        }
    }
}