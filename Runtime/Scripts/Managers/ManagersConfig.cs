﻿using System.Collections.Generic;
using UnityEngine;

namespace Playstrom.Core
{
    [CreateAssetMenu(menuName = Constants.MANAGERS + Constants.SLASH + Constants.MANAGERS_CONFIG, fileName = Constants.MANAGERS_CONFIG)]
    public class ManagersConfig : ScriptableObject
    {
        [SerializeField] private BaseManager[] managers;

        public void Init()
        {
            for (int i = 0; i < managers.Length; i++)
            {
                managers[i].Init();
            }
        }

    }
}